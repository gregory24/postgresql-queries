-- NOT Empty
CREATE TABLE check_data1 AS (
    SELECT * FROM (VALUES (1)) AS vals(val)
);

-- Empty
CREATE TABLE check_data2 (val INT);

-- NOT Empty
CREATE TABLE check_data3 AS (
    SELECT * FROM (VALUES (2, 3)) AS vals(val)
);

-- Empty
CREATE TABLE check_data4 (val INT);

-- Checking which ones of the provided tables are not empty
SELECT ARRAY_AGG(table_key) AS non_empty_table_keys FROM (
    SELECT * FROM (SELECT 'table-1' AS table_key FROM check_data1 LIMIT 1) AS check1
    UNION ALL
    SELECT * FROM (SELECT 'table-2' AS table_key FROM check_data2 LIMIT 1) AS check2
    UNION ALL
    SELECT * FROM (SELECT 'table-3' AS table_key FROM check_data3 LIMIT 1) AS check3
    UNION ALL
    SELECT * FROM (SELECT 'table-4' AS table_key FROM check_data4 LIMIT 1) AS check4
) AS all_checks;