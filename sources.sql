------------------------------------------------------------------------------------------------------------------------
-- Tables
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE source_data (key INTEGER, external_id TEXT);

CREATE TABLE object_data (key INTEGER, sources JSONB);

------------------------------------------------------------------------------------------------------------------------
-- Data
------------------------------------------------------------------------------------------------------------------------

TRUNCATE source_data;

INSERT INTO source_data(key, external_id) VALUES (1, 'id-1'), (1, 'id-2'), (1, 'id-3');

TRUNCATE object_data;

-- No sources -> Empty Sources Object
INSERT INTO object_data(key, sources) VALUES
(1, '{"source1": {"external-1":  true, "external-2":  false}, "source2": {"external-1":  false, "external-2":  true}}');

------------------------------------------------------------------------------------------------------------------------
-- Adding External-Ids for a source
------------------------------------------------------------------------------------------------------------------------

-- Deduplication (Would be done by Window-Function instead)
--   -> Adding to the last step

DROP TABLE IF EXISTS update_data;

CREATE TABLE update_data AS (
    SELECT key, jsonb_build_object('source1', external_ids) AS sources FROM (
        SELECT key, JSON_OBJECT_AGG(external_id, TRUE) AS external_ids FROM source_data
        GROUP BY key
    ) AS data
);

SELECT sources FROM update_data;

-- Update (Insertion would just be inserting the records)

SELECT
    object_data.key,
    JSONB_SET(
        object_data.sources,
        '{source1}',
        COALESCE((object_data.sources->'source1')::JSONB, '{}'::JSONB)
        ||
        (update_data.sources->'source1')::JSONB
    )
FROM update_data
INNER JOIN object_data ON update_data.key = object_data.key;

SELECT 1 != COALESCE(NULL, 1);

------------------------------------------------------------------------------------------------------------------------
-- Removing External-Ids from a source
------------------------------------------------------------------------------------------------------------------------

-- Deduplication (Would be done by Window-Function instead)
--   -> Adding to the last step

DROP TABLE IF EXISTS deletion_data;

CREATE TABLE deletion_data AS (
    SELECT key, jsonb_build_object('source1', external_ids) AS sources FROM (
        SELECT key, JSON_OBJECT_AGG(external_id, FALSE) AS external_ids FROM source_data
        GROUP BY key
    ) AS data
);

SELECT sources FROM deletion_data;

-- Deletion (Setting to False)

SELECT
    object_data.key,
    JSONB_SET(
        object_data.sources,
        '{source1}',
        COALESCE((object_data.sources->'source1')::JSONB, '{}'::JSONB)
        ||
        (deletion_data.sources->'source1')::JSONB
    )
FROM deletion_data
INNER JOIN object_data ON deletion_data.key = object_data.key;

------------------------------------------------------------------------------------------------------------------------
-- Retrieving IDs for each source
------------------------------------------------------------------------------------------------------------------------

-- TODO: Update

SELECT
key,
ARRAY_AGG(source1_data->>'key') FILTER (WHERE (source1_data->>'value')::BOOLEAN) AS source1_ids,
ARRAY_AGG(source2_data->>'key') FILTER (WHERE (source2_data->>'value')::BOOLEAN) AS source2_ids
FROM (
    SELECT
           key,
           ROW_TO_JSON(JSONB_EACH(sources->'source1')) AS source1_data,
           ROW_TO_JSON(JSONB_EACH(sources->'source2')) AS source2_data
    FROM object_data
) AS object_with_sources
GROUP BY key;
