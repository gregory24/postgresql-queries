WITH check_values AS (
    SELECT DISTINCT ON (master_id) * FROM (
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE EXISTS(
            SELECT 1 FROM input_data_table
            WHERE object_data_table.key1_1 = input_data_table.key1_1
            AND object_data_table.key1_2 = input_data_table.key1_2
        )
        UNION ALL
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE key2 IN (SELECT key2 FROM input_data_table)
        UNION ALL
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE key3 IN (SELECT key3 FROM input_data_table)
        UNION ALL
        SELECT master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3, __request_id, __timestamp, 'new' AS __label
        FROM input_data_table
    ) AS all_candidates
    ORDER BY master_id, __timestamp DESC -- Taking the most recent versions of records
), violations AS (
    SELECT request_id, ARRAY_AGG(key_name) AS keys FROM (
        SELECT current_request AS request_id, 'key1' AS key_name FROM (
            SELECT
                root_key,

                UNNEST(keys) AS current_key,
                UNNEST(requests) AS current_request
            FROM (
                SELECT
                    (ARRAY_AGG((main_key_1)::TEXT ORDER BY __timestamp))[1] AS root_key,

                    (ARRAY_AGG((main_key_1, main_key_2)::TEXT ORDER BY __timestamp))[2:] AS keys,
                    (ARRAY_AGG(__request_id ORDER BY __timestamp))[2:] AS requests
                FROM check_values
                GROUP BY key1_1, key1_2
                HAVING COUNT(1) > 1
            ) AS candidates
        ) AS parsed_candidates
        WHERE root_key != current_key
        UNION ALL
        SELECT current_request AS request_id, 'key2' AS key_name FROM (
            SELECT
                root_key,

                UNNEST(keys) AS current_key,
                UNNEST(requests) AS current_request
            FROM (
                SELECT
                    (ARRAY_AGG((main_key_1, main_key_2)::TEXT ORDER BY __timestamp))[1] AS root_key,

                    (ARRAY_AGG((main_key_1, main_key_2)::TEXT ORDER BY __timestamp))[2:] AS keys,
                    (ARRAY_AGG(__request_id ORDER BY __timestamp))[2:] AS requests
                FROM check_values
                GROUP BY key2
                HAVING COUNT(1) > 1
            ) AS candidates
        ) AS parsed_candidates
        WHERE root_key != current_key
        UNION ALL
        SELECT current_request AS request_id, 'key3' AS key_name FROM (
            SELECT
                root_key,

                UNNEST(keys) AS current_key,
                UNNEST(requests) AS current_request
            FROM (
                SELECT
                    (ARRAY_AGG((main_key_1, main_key_2)::TEXT ORDER BY __timestamp))[1] AS root_key,

                    (ARRAY_AGG((main_key_1, main_key_2)::TEXT ORDER BY __timestamp))[2:] AS keys,
                    (ARRAY_AGG(__request_id ORDER BY __timestamp))[2:] AS requests
                FROM check_values
                GROUP BY key3
                HAVING COUNT(1) > 1
            ) AS candidates
        ) AS parsed_candidates
        WHERE root_key != current_key
    ) AS all_violations
    GROUP BY request_id
), deletions AS (
    DELETE FROM input_data_table
    WHERE __request_id IN (SELECT __request_id FROM violations)
)
SELECT * FROM violations;
-- INNER JOIN input_data_table ON request_id = __request_id; -- For checking