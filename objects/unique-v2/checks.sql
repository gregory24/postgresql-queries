--------------------------------------------------------------------------------
-- Finding violations JUST for key1 - Same patter for other keys
--------------------------------------------------------------------------------

-- 1) Finding Candidates for Uniqueness-Violation
--      a) Finding Root-Record (The one with original value) - Could be from the Object or the incoming Data
--      b) Finding Violation-Records (Those other than the first one)
--    -> Only for Keys that are encountered more than once (At least 2)

DROP TABLE IF EXISTS candidates;

CREATE TEMPORARY TABLE candidates AS (
    SELECT
        key1_1,
        key1_2,

        (ARRAY_AGG(((main_key_1, main_key_2)::TEXT) ORDER BY __timestamp))[1] AS root_key,
        (ARRAY_AGG(__request_id ORDER BY __timestamp))[1] AS root_request,
        (ARRAY_AGG(__label ORDER BY __timestamp))[1] AS root_label,

        (ARRAY_AGG(((main_key_1, main_key_2)::TEXT) ORDER BY __timestamp))[2:] AS keys,
        (ARRAY_AGG(__request_id ORDER BY __timestamp))[2:] AS requests,
        (ARRAY_AGG(__label ORDER BY __timestamp))[2:] AS labels
    FROM check_table
    GROUP BY key1_1, key1_2
    HAVING COUNT(1) > 1
);

SELECT * FROM candidates;

-- 2) Parsing Candidates into separate records with associated Root data

DROP TABLE IF EXISTS parsed_candidates;

CREATE TEMPORARY TABLE parsed_candidates AS (
    SELECT
        key1_1,
        key1_2,

        root_key,
        root_request
        root_label,

        unnest(keys) AS current_key,
        unnest(requests) AS current_request,
        unnest(labels) AS current_label
    FROM candidates
);

SELECT * FROM parsed_candidates;

-- 3) Filtering out NON-Violating Records - Those that are going to to UPDATE the Root-Record
--       -> Leaving only Violating Records

DROP TABLE IF EXISTS violators;

CREATE TEMPORARY TABLE violators AS (
    SELECT key1_1, key1_2, current_key, current_request, current_label FROM parsed_candidates
    WHERE root_key != current_key
);

SELECT * FROM violators;