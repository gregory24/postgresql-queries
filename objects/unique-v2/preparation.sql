-- Finding all records from Object-Table which have matching unique-key values -> Could be possible violation-detectors

-- SET enable_hashjoin = FALSE;
-- SET enable_mergejoin = FALSE;

DROP TABLE IF EXISTS check_table;

-- Internals: Parallel-Append with Parallel-Workers
--    Object-Table: Searching using Nested-Loop-Joins on Hash-Aggregates from Input-Data-Table (Removing duplicates)
--    Input-Data-Table: Sequential Scan
-- SUPER-PERFORMANT: Nested-Loops work in parallel

-- SUPER-IMPORTANT: Ordering is REQUIRED -> Choosing the most recent versions of records => Not missing anything
CREATE TABLE check_table AS (
    SELECT DISTINCT ON (master_id) * FROM (
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE EXISTS(
            SELECT 1 FROM input_data_table
            WHERE object_data_table.key1_1 = input_data_table.key1_1
            AND object_data_table.key1_2 = input_data_table.key1_2
        )
        UNION ALL
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE key2 IN (SELECT key2 FROM input_data_table)
        UNION ALL
        SELECT
            master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3,
            (NULL::UUID) AS __request_id,
            TO_TIMESTAMP(0) AS __timestamp,
            'original' AS __label
        FROM object_data_table
        WHERE key3 IN (SELECT key3 FROM input_data_table)
        UNION ALL
        SELECT master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3, __request_id, __timestamp, 'new' AS __label
        FROM input_data_table
    ) AS all_candidates
    ORDER BY master_id, __timestamp DESC -- Taking the most recent versions of records
);

SELECT * FROM check_table;
