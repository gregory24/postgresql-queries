------------------------------------------------------------------------------------------------------------
-- Common tables and indexes
------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS object_data_table;

CREATE TABLE object_data_table (
    master_id UUID NOT NULL,
    main_key_1 INTEGER NOT NULL,
    main_key_2 INTEGER NOT NULL,
    key1_1 INTEGER NOT NULL,
    key1_2 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    key3 INTEGER NOT NULL,
    info1 TEXT,
    info2 TEXT,
    info3 TEXT
);

CREATE INDEX main_key_idx ON object_data_table(main_key_1, main_key_2);
CREATE INDEX key1_idx ON object_data_table(key1_1, key1_2);
CREATE INDEX key2_idx ON object_data_table(key2);
CREATE INDEX key3_idx ON object_data_table(key3);

DROP TABLE IF EXISTS input_data_table;

CREATE TABLE input_data_table (
    master_id UUID NOT NULL,
    main_key_1 INTEGER NOT NULL,
    main_key_2 INTEGER NOT NULL,
    key1_1 INTEGER NOT NULL,
    key1_2 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    key3 INTEGER NOT NULL,
    info1 TEXT,
    info2 TEXT,
    info3 TEXT,
    __request_id UUID NOT NULL,
    __timestamp TIMESTAMPTZ NOT NULL
);

------------------------------------------------------------------------------------------------------------
-- Data for checking and testing logic / results - FORMAT 2 (Separated into tables)
------------------------------------------------------------------------------------------------------------

TRUNCATE object_data_table;

INSERT INTO object_data_table
    (master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3, info1, info2, info3)
VALUES
        (gen_random_uuid(), 1, 1, 10, 10, 100, 1000, '-', '-', '-'),
        (gen_random_uuid(), 2, 2, 20, 20, 200, 2000, '-', '-', '-'),
        (gen_random_uuid(), 3, 3, 30, 30, 300, 3000, '-', '-', '-');

TRUNCATE input_data_table;

INSERT INTO input_data_table
    (master_id, main_key_1, main_key_2, key1_1, key1_2, key2, key3, info1, info2, info3, __request_id, __timestamp)
VALUES
       (gen_random_uuid(), 1, 1, 20, 20, 300, 1000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(1)),
       (gen_random_uuid(), 4, 4, 40, 40, 400, 4000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(2)),
       (gen_random_uuid(), 4, 4, 30, 30, 400, 2000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(3)),
       (gen_random_uuid(), 2, 2, 20, 20, 500, 5000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(4)),
       (gen_random_uuid(), 3, 3, 10, 10, 100, 1000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(5)),
       (gen_random_uuid(), 3, 3, 30, 30, 600, 3000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(6)),
       (gen_random_uuid(), 5, 5, 20, 20, 500, 5000, '-', '-', '-', gen_random_uuid(), TO_TIMESTAMP(7));

------------------------------------------------------------------------------------------------------------
-- Data for preparation and performance testing (1K -> 1M)
------------------------------------------------------------------------------------------------------------

TRUNCATE object_data_table;

INSERT INTO object_data_table
SELECT
    gen_random_uuid(),
    i,
    i,
    i * 10,
    i * 10,
    i * 100,
    i * 1000,
    CAST(i * 10 AS TEXT),
    CAST(i * 100 AS TEXT),
    CAST(i * 1000 AS TEXT)
FROM generate_series(1, 1000 * 1000) AS series(i);

-- WORST-CASE: ALL VIOLATIONS

TRUNCATE input_data_table;

INSERT INTO input_data_table
SELECT
    gen_random_uuid(),
    ((i * (i + 135) + ((i + 11) % 10)) % 1000),
    ((i * (i + 135) + ((i + 11) % 10)) % 1000),
    ((i * ((i + 433) % 10) + (i + 811)) % 1000) * 10,
    ((i * ((i + 433) % 10) + (i + 811)) % 1000) * 10,
    ((i * (i + 33) + ((i + 5) % 555)) % 1000) * 100,
    ((i * ((i + 342) % 10) + (i + 622)) % 1000) * 1000,
    CAST(i * 10 AS TEXT),
    CAST(i * 100 AS TEXT),
    CAST(i * 1000 AS TEXT),
    gen_random_uuid() AS __request_id,
    TO_TIMESTAMP(i * 1000) AS __timestamp
FROM generate_series(1, 1000) AS series(i);

SELECT * FROM input_data_table;
