------------------------------------------------------------------------------------------------------------
-- Data for checking and testing logic / results - FORMAT 1 (United in a table)
------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS check_table;

CREATE TABLE check_table (
    main_key INTEGER NOT NULL,
    key1 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    key3 INTEGER NOT NULL,
    __request_id UUID NOT NULL,
    __timestamp TIMESTAMPTZ NOT NULL,
    __label TEXT NOT NULL -- Just for debugging (Since RID is not that good)
);

TRUNCATE check_table;

INSERT INTO check_table(main_key, key1, key2, key3, __request_id, __timestamp, __label)
VALUES
    (1, 10, 100, 1000, gen_random_uuid(), TO_TIMESTAMP(0), '1 - OBJECT'),
    (2, 20, 200, 2000, gen_random_uuid(), TO_TIMESTAMP(0), '2 - OBJECT'),
    (3, 30, 300, 3000, gen_random_uuid(), TO_TIMESTAMP(0), '3 - OBJECT'),
    (1, 20, 300, 1000, gen_random_uuid(), TO_TIMESTAMP(1), '1 - DATA'),
    (4, 40, 400, 4000, gen_random_uuid(), TO_TIMESTAMP(2), '4 - DATA-ORIGINAL'),
    (4, 30, 400, 2000, gen_random_uuid(), TO_TIMESTAMP(3), '4 - DATA-NEW'),
    (2, 20, 500, 5000, gen_random_uuid(), TO_TIMESTAMP(4), '2 - DATA'),
    (3, 10, 100, 1000, gen_random_uuid(), TO_TIMESTAMP(5), '3 - DATA-1'),
    (3, 30, 600, 3000, gen_random_uuid(), TO_TIMESTAMP(6), '3 - DATA-2'),
    (5, 20, 500, 5000, gen_random_uuid(), TO_TIMESTAMP(7), '5 - DATA');