CREATE TABLE search_table (
    master_id UUID NOT NULL,
    key INTEGER NOT NULL,
    info TEXT
);

-- 1M records (10 matches per key for heavy-load simulation)
DROP INDEX IF EXISTS search_key_idx;

TRUNCATE search_table;
INSERT INTO search_table
SELECT gen_random_uuid(), (i % 100000), i::TEXT FROM generate_series(1, 1000 * 1000) AS series(i);

CREATE INDEX IF NOT EXISTS search_key_idx ON search_table USING BTREE (key);

-- 200 records
DROP TABLE IF EXISTS search_data;
CREATE TABLE search_data AS (
    SELECT key FROM search_table TABLESAMPLE bernoulli(0.004)
);

SELECT COUNT(1) FROM search_data;

-- Disabling for Search-Operation to FULLY utilize Index-Scans with Nested-Loops (BEST PERFORMANCE)
SET enable_hashjoin = TRUE;
SET enable_mergejoin = TRUE;

-- Phase 1: Simple filtering
--    Default: Hash-Join -> OK (Hashing the input-table / key-table)
--    Without Hash-Joins:
--        a) Nested-Loop on Index-Scans / Bitmap-Index-Scans -> GOOD
--        b) Merge-Join by Index-Scan -> OK
EXPLAIN
SELECT * FROM search_table
WHERE key IN (SELECT key FROM search_data);

-- Phase 2: Grouping results - Better than In-Memory
-- Time: <0.5s -> Sufficient enough

-- Version 1
EXPLAIN
SELECT key, JSON_AGG(filtered_data.*) FROM (
    SELECT * FROM search_table
    WHERE key IN (SELECT key FROM search_data)
) AS filtered_data
GROUP BY key;

-- Version 2: Same query plan - Easier syntax
EXPLAIN
SELECT key, JSON_AGG(search_table.*) FROM search_table
WHERE key IN (SELECT key FROM search_data)
GROUP BY key;

-- Version 3: THE WORST - CREATING A TMP-TABLE WITH SUCH VOLUME OF DATA IS BAD

BEGIN;

CREATE TEMPORARY TABLE filtered_data ON COMMIT DROP AS (
    SELECT * FROM search_table
    WHERE key IN (SELECT key FROM search_data)
);

CREATE INDEX tmp_idx ON filtered_data USING BTREE (key);

SELECT key, JSON_AGG(filtered_data.*) FROM filtered_data GROUP BY key;

COMMIT;
