DROP TABLE IF EXISTS index_tester;

CREATE TABLE index_tester AS
SELECT
       i % 1030 AS a,
       (i + 1) % 1500 AS b,
       (i + 2) % 1700 AS c,
       i AS d
FROM generate_series(1, 1000 * 1000) AS series(i);

DROP INDEX IF EXISTS test_index_1;
DROP INDEX IF EXISTS test_index_2;

CREATE INDEX test_index_1 ON index_tester USING BTREE(a, b, c);
CREATE INDEX test_index_2 ON index_tester USING BTREE(d);

-- Index-Scans:
--   Index-Scan:
--        Cases: Used in most cases when Index can optimize Sorting or Searching, but Table-Data is also needed
--        Usage:
--          a) Sorting: Scanning the Index which is sorted according to a key => Index-Scan (Always used)
--          b) Searching: Applying conditions on the Index that are getting Scanned => Index-Scan with Index-Cond
--          c) Filtering: Applying conditions on Table (Non-Indexed Columns) => Index-Scan with Filter
--            -> Retrieving data from the Table anyway (Why not apply extra conditions apart from Index-related ones)
--        Internals: Scanning Index for Sorting / Search with Conditions -> Loading Records from the Table
--   Index-Only-Scan:
--        Cases: Used only when the entire Query can be satisfied by an Index
--        Usage: Same as Index-Scan but when NO Columns outside of Index are used (Neither Output, Searching or Sorting)
--        Internals: Scanning Index for Sorting / Search with Conditions without loading Records from the Table
--        Note: If a Non-Index column is used for Filtering, a Index-Scan is used instead (Fetching extra data in Table)
--   Bitmap-Index-Scan: Used when
--        Cases:
--          a) Combining multiple Index together by using Conditions of each one of them for Searching (NOT SORTING)
--          b) Dealing with Large Outputs (Simply scanning a part of a Table instead of a whole one)
--        Usage:
--          a) Sorting: Applied afterwards as an additional Sort-Operation (NOT automatically provided)
--          b) Searching: Applying Conditions supported by an Index on the Index-Scan and Rechecking them on Heap-Scan
--          c) Filtering: Applying additional Conditions to be applied with Heap-Scan (Working with full Rows anyway)
--        Internals:
--          1) Creating a Bitmap of ALL Pages of a Table by using Index-Scan with Index-Condition for each Index Used
--               - Setting "1" if a Page contains Records with matching Conditions and settings "0" otherwise
--          2) Combining Bitmaps together using Bitwise-Or / Bitwise-And depending on the Combining-Operators used
--               - Bitmap-Or is extensively used for combining, while Bitmap-And is mostly ignored for better Plans
--          3) Scanning through retrieved Pages and Filtering out Records based on Index and Non-Index Conditions
--               a) Rechecking Records using Index-Conditions
--               b) Applying additional Filtering
--          4) Applying Sorting if needed - NOT provided by the Index, since the output is NOT Sorted / Ordered
--               - Using a specific Sort-Operation instead
--        Note: Only Searching / Filtering is provided by Bitmap-Index-Scan (Output is unordered) -> Sorting is extra
--   Important: Multiple Indexes can be combined ONLY with Searching with them through Bitmap-Index-Scan

-- Index-Scan / Bitmap-Index-Scan with Index-Cond (Using Index for Searching / Filtering)
------------------------------------------------------------------
-- Index-Scan with Matching Operations
EXPLAIN
SELECT * FROM index_tester
WHERE a = 10 AND b = 11;

-- Bitmap-Index-Scan with Comparison Operations
EXPLAIN
SELECT * FROM index_tester
WHERE a BETWEEN 950 AND 1000 AND b > 800 AND c < 1000;

-- Index-Only-Scan with Comparison Operations
EXPLAIN
SELECT a, b, c FROM index_tester
WHERE a BETWEEN 950 AND 1000 AND b > 800 AND c < 1000;

-- Index-Scan with SMALL Set-Operations (Only Positive)
--     - NOT IN / != ANY Operations are NOT supported
EXPLAIN
SELECT * FROM index_tester
WHERE a = ANY(ARRAY[800, 900, 1000]) AND b IN (700, 800, 900);

-- Index-Only-Scan when outputting Index-Only columns and NOT using Non-Index Columns for Filtering
EXPLAIN
SELECT a, b FROM index_tester
WHERE a = 10 AND b = 11;


-- Index-Scan with Index-Cond and Filter (Using Index for Searching / Filtering)
-- Index-Condition and Filter can be used together:
--    1) Scanning based on the Condition supported by the Index
--    2) Additionally Filtering out based on Non-Index Conditions
------------------------------------------------------------------
EXPLAIN
SELECT * FROM index_tester
WHERE a = 10 AND b = 11 AND d = 618010;

-- Ordering does NOT matter
EXPLAIN
SELECT * FROM index_tester
WHERE d = 618010 AND a = 10 AND b = 11;

-- Index-Only-Scan turns into Index-Scan when Filtering is used (Retrieving data from the Table for Filtering)
EXPLAIN
SELECT a, b FROM index_tester
WHERE a = 10 AND b = 11 AND d = 618010;


-- Index-Scan with Filtering (Using Index for Sorting / Ordering WITHOUT Index-Condition)
------------------------------------------------------------------
-- Scenario 1: Using Non-Index Columns for Searching
EXPLAIN
SELECT * FROM index_tester
WHERE d > 50000
ORDER BY a;

-- Scenario 2: Using Operations NOT supported by the Index
-- Supported:
--   1) Matching / Not-Matching: = / !=
--   2) Comparison: > / < / >= / <= / BETWEEN
--   3) Small Subsets: IN / ANY (Just Positive)
EXPLAIN
SELECT * FROM index_tester
WHERE a NOT IN (500, 800)
ORDER BY a;

-- Scenario 3: Using Multi-Condition with OR
EXPLAIN
SELECT * FROM index_tester
WHERE (a > 500) OR (b < 400)
ORDER BY a;


-- Bitmap-Index-Scans
-- Data: Using Range / Comparison Conditonis for LARGER Output to provoke Bitmap-Index-Scan ()
------------------------------------------------------------------

-- Combining 2 Bitmap-Index-Scans with Bitmap-Or
-- Flow: Bitmap-Index-Scan (a) + Bitmap-Index-Scan (d) -> Bitmap-Or -> Rechecking -> Bitmap-Heap-Scan
EXPLAIN
SELECT * FROM index_tester
WHERE a = 100
OR d BETWEEN 100000 AND 100500;

-- Combining 3 Bitmap-Index-Scans with Bitmap-Or
-- Note: Can use the same Index multiple times for varying conditions
EXPLAIN
SELECT * FROM index_tester
WHERE a BETWEEN 100 AND 200
OR d BETWEEN 100000 AND 101000
OR d BETWEEN 200000 AND 202000;

-- Combining 2 Bitmap-Index-Scans with Bitmap-Or and applying additional Filter ABOVE Bitmap-Index-Scans and Bitmap-Or
-- Flow: Bitmap-Index-Scan (a) + Bitmap-Index-Scan (d) -> Bitmap-Or -> Rechecking and FILTERING -> Bitmap-Heap-Scan
-- Important: Group Bitmap-Conditions together for Filtering to be applied ABOVE (At Rechecking / Bitmap-Heap-Scan step)
EXPLAIN
SELECT * FROM index_tester
WHERE (a = 100 OR d BETWEEN 100000 AND 100500) -- Bitmap-Index-Scans -> Bitmap-Or
AND d NOT IN (100100, 100200, 100300, 100400); --NOT supported by Indexes -> Filter
