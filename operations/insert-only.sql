------------------------------------------------------------------------------------------------------------------------
-- Tables
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE target(
    key1 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    info TEXT
);

CREATE TABLE input(LIKE target);

------------------------------------------------------------------------------------------------------------------------
-- Data
------------------------------------------------------------------------------------------------------------------------

TRUNCATE target;

INSERT INTO target(key1, key2, info)
SELECT i * 5, i * 7, i::TEXT FROM generate_series(1, 1000 * 1000) AS series(i);

TRUNCATE input;

INSERT INTO input(key1, key2, info)
SELECT i, i, i::TEXT FROM generate_series(1, 1000 * 1000) AS series(i);

------------------------------------------------------------------------------------------------------------------------
-- Constraints (Uniqueness / Indexes)
------------------------------------------------------------------------------------------------------------------------

DROP INDEX IF EXISTS target_key1;
-- DROP INDEX IF EXISTS target_key2;

CREATE UNIQUE INDEX target_key1 ON target USING BTREE (key1);
-- CREATE UNIQUE INDEX target_key2 ON target USING BTREE (key2);

------------------------------------------------------------------------------------------------------------------------
-- Approach 1: Anti-Join -> TMP -> Insertion
------------------------------------------------------------------------------------------------------------------------

-- ALWAYS TWICE FASTER (UNDER ANY LOAD)

SET enable_hashjoin = FALSE;

DROP TABLE IF EXISTS insertion_data1;

-- Time: 8s, 6.5s, 6s
BEGIN;

-- 600ms
CREATE INDEX input_key1 ON input USING BTREE (key1);

-- No Index: 1.5ms -> Merge-Join(Materialized-Sort + Index-Only-Scan)
-- With Index: 700ms -> Merge-Join(Index-Scan + Index-Only-Scan)
CREATE TABLE insertion_data1 AS (
    SELECT key1, key2, info FROM (
        SELECT
           input.*,
           (CASE
               WHEN target.key1 IS NULL THEN TRUE
               ELSE FALSE
            END) AS is_new
        FROM input
        LEFT JOIN target ON input.key1 = target.key1
    ) AS data_with_match_info
    WHERE is_new IS TRUE
);

-- 3.5s
INSERT INTO target
SELECT * FROM insertion_data1;

DROP INDEX input_key1;

COMMIT;

--
SELECT COUNT(1) FROM insertion_data1;

------------------------------------------------------------------------------------------------------------------------
-- Approach 2: Insertion with Ignores (Uniqueness)
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS insertion_data2;

-- Time: ~7.5s, 8s
CREATE TABLE insertion_data2 AS (
    WITH insertion_results AS (
        INSERT INTO target
        SELECT * FROM input
        ON CONFLICT DO NOTHING
        RETURNING key1, key2, info
    )
    SELECT * FROM insertion_results
);

-- 800000
SELECT COUNT(1) FROM insertion_data1;
