-- Source and Destination Tables: 1M Records each
--    Source: 0 -> 1,000,000 (All) => NOT Indexed on PK (Neither Regular, Unique nor Primary-Key Index)
--    Destination: 0 -> 2,000,000 (Even) => Uniquely Indexes on PK (Primary-Key Index)

DROP TABLE IF EXISTS destination_table;
DROP TABLE IF EXISTS source_table;

CREATE TABLE destination_table (
    pk INTEGER NOT NULL,

    pk1 INTEGER NOT NULL,
    pk2 INTEGER NOT NULL,

    info1 INTEGER NOT NULL,
    info2 VARCHAR NOT NULL,
    info3 TEXT NOT NULL,

    CONSTRAINT destination_table_real_pk PRIMARY KEY (pk1, pk2)
);

-- Constraints can be BOTH Included and Excluded
--   -> Excluding to NOT have Primary Key / Indexes
CREATE TABLE source_table (LIKE destination_table EXCLUDING CONSTRAINTS);

-- Simulating 2 Primary Keys (Single-Key and Multi-Key)
-- Single-Key
DROP INDEX destination_table_pk;
CREATE UNIQUE INDEX destination_table_pk ON destination_table USING BTREE (pk);
-- Multi-Key
DROP INDEX destination_table_multi_pk;
CREATE UNIQUE INDEX destination_table_multi_pk ON destination_table USING BTREE (pk1, pk2);


-- Populating with Data
---------------------------

TRUNCATE source_table;
INSERT INTO source_table(pk, pk1, pk2, info1, info2, info3)
SELECT i, i, i, i, i || ':char', i || ':text' FROM generate_series(0, 10 * 1000 * 1000, 1) AS series(i);

TRUNCATE destination_table;
INSERT INTO destination_table(pk, pk1, pk2, info1, info2, info3)
SELECT i, i, i, i, i || ':char', i || ':text' FROM generate_series(0, 20 * 1000 * 1000, 2) AS series(i);
