-- Finding Matches
-------------------------
-------------------------

-- Single-Key (No Parallel Workers)
--    With Index: <1s
--    Without Index: ~1s
BEGIN;

CREATE UNIQUE INDEX source_table_pk ON source_table USING BTREE (pk);

CREATE TEMPORARY TABLE matches ON COMMIT DROP AS (
    SELECT pk FROM source_table
    WHERE EXISTS (SELECT 1 FROM destination_table WHERE source_table.pk = destination_table.pk)
);

DROP INDEX source_table_pk;

COMMIT;


-- Multi-Key (Launching Parallel Workers)
--    With Index: <1s
--    Without Index: <1s
BEGIN;

CREATE UNIQUE INDEX source_table_multi_pk ON source_table USING BTREE (pk1, pk2);

CREATE TEMPORARY TABLE matches ON COMMIT DROP AS (
    SELECT pk1, pk2 FROM source_table
    WHERE EXISTS (
        SELECT 1 FROM destination_table
        WHERE source_table.pk1 = destination_table.pk1
        AND source_table.pk2 = destination_table.pk2
    )
);

DROP INDEX source_table_multi_pk;

COMMIT;


-- Optimizations
------------------------

-- Single-Key
DROP INDEX IF EXISTS source_table_pk;
CREATE UNIQUE INDEX source_table_pk ON source_table USING BTREE (pk);
-- Multi-Key
DROP INDEX IF EXISTS source_table_multi_pk;
CREATE UNIQUE INDEX source_table_multi_pk ON source_table USING BTREE (pk1, pk2);


-- Performance
------------------------

-- Testing (IN)
EXPLAIN ANALYZE
SELECT pk FROM source_table
WHERE pk IN (SELECT pk FROM destination_table);

-- Single-Key (IN)
--    No Index: Seq-Scan (Source) + Seq-Scan (Destination) + Hash-Join -> 37s
--    With Index: Index-Only-Scan (Source) + Index-Only-Scan (Destination) + Merge-Join -> 27s


-- Testing (EXISTS with Single-Key)
EXPLAIN ANALYZE
SELECT pk FROM source_table
WHERE EXISTS (
    SELECT 1 FROM destination_table
    WHERE source_table.pk = destination_table.pk
);

-- Single-Key (EXISTS) -> Identical to IN plans
--   No Index: Seq-Scan (Source) + Seq-Scan (Destination) + Hash-Join -> 36s
--   With Index: Index-Only-Scan (Source) + Index-Only-Scan (Destination) + Merge-Join -> 26s


-- Testing (EXISTS with Multi-Key)
EXPLAIN ANALYZE
SELECT pk1, pk2 FROM source_table
WHERE EXISTS (
    SELECT 1 FROM destination_table
    WHERE source_table.pk1 = destination_table.pk1
    AND source_table.pk2 = destination_table.pk2
);

-- Multi-Key (EXISTS)
--   No Index (Parallel): 15s
--   With Index (Parallel): 15s

SET enable_seqscan = FALSE;
SET enable_seqscan = TRUE;
