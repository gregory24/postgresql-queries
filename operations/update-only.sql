------------------------------------------------------------------------------------------------------------------------
-- Tables
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE update_target(
    key1 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    info TEXT
);

CREATE TABLE update_input(LIKE target);

------------------------------------------------------------------------------------------------------------------------
-- Data
------------------------------------------------------------------------------------------------------------------------

-- 5K matching

TRUNCATE update_target;

INSERT INTO update_target(key1, key2, info)
SELECT i * 2, i * 2, (-1)::TEXT FROM generate_series(1, 1000 * 1000) AS series(i);

TRUNCATE update_input;

INSERT INTO update_input(key1, key2, info)
SELECT i, i, (i % 3)::TEXT FROM generate_series(495 * 1000, 505 * 1000) AS series(i);

------------------------------------------------------------------------------------------------------------------------
-- Constraints (Uniqueness / Indexes)
------------------------------------------------------------------------------------------------------------------------

DROP INDEX IF EXISTS update_target_key;

CREATE UNIQUE INDEX update_target_key ON update_target USING BTREE (key1, key2);

------------------------------------------------------------------------------------------------------------------------
-- Joining
------------------------------------------------------------------------------------------------------------------------

SET enable_hashjoin = FALSE;

DROP TABLE IF EXISTS update_table;

BEGIN;

-- Time: ~700ms
CREATE UNIQUE INDEX update_input_key ON update_input USING BTREE (key1, key2);

-- Time:
--   With Index: ~4s
--   Without Index: ~5s
-- Plan: Merge-Join on Index-Scans (Asking more data for updates and checks)
EXPLAIN
CREATE TABLE update_table AS (
    WITH update_results AS (
        UPDATE update_target
        SET info = update_input.info
        FROM update_input
        WHERE update_target.key1 = update_input.key1 AND update_target.key2 = update_input.key2 -- Key-Matching
        AND update_target.info != update_input.info
        RETURNING update_target.*
    )
    SELECT * FROM update_results
);

DROP INDEX update_input_key;

COMMIT;

SELECT COUNT(1) FROM update_table;
