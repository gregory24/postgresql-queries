SET enable_seqscan = TRUE;
SET enable_seqscan = FALSE;

-- Preemptive Optimization
DROP INDEX IF EXISTS source_pk;
CREATE INDEX IF NOT EXISTS source_pk ON source_table USING BTREE (pk1, pk2);

BEGIN;

-- 2-3s
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

-- 8s
CREATE TEMPORARY TABLE matches_table ON COMMIT DROP AS (
    SELECT * FROM (
        SELECT
           source_table.*,
           (CASE
               WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN TRUE
               ELSE FALSE
            END) AS is_new
        FROM source_table
        LEFT JOIN destination_table
            ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
    ) AS data_with_match_info
    WHERE is_new IS TRUE
);

-- 20s
UPDATE destination_table
SET info1 = source_table.info1, info2 = source_table.info2, info3 = source_table.info3
FROM source_table
WHERE destination_table.pk1 = source_table.pk1 AND destination_table.pk2 = source_table.pk2;

CREATE INDEX is_new_index ON matches_table (is_new);

-- 10s
INSERT INTO destination_table
SELECT pk1, pk2, info1, info2, info3 FROM matches_table;

-- 1-2s
DROP INDEX source_pk;

COMMIT;


-- Testing Performance
---------------------------

SET enable_seqscan = FALSE;
SET enable_seqscan = TRUE;

-- Setup

CREATE UNIQUE INDEX IF NOT EXISTS source_pk ON source_table USING BTREE (pk1, pk2);

DROP TABLE IF EXISTS matches_table;
CREATE TABLE matches_table AS (
    SELECT
       source_table.pk1 AS pk1,
       source_table.pk2 AS pk2,
       (CASE
           WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN FALSE
           ELSE TRUE
        END) AS is_matching
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
);

CREATE UNIQUE INDEX IF NOT EXISTS matches_table_index ON matches_table USING BTREE (pk1, pk2, is_matching);


-- Matches: ~8s (Using Indexes regardless of Seq-Scan Settings)
EXPLAIN
CREATE TABLE IF NOT EXISTS matches_table_p AS (
    SELECT
       source_table.*,
       (CASE
           WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN FALSE
           ELSE TRUE
        END) AS is_matching
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
);

DROP TABLE IF EXISTS matches_table_p;


-- Update:
--    Seq-Scan On: ~1m (Using Seq-Scans)
--    Seq-Scan Off: ~15s (Using Indexes)
EXPLAIN
UPDATE destination_table
SET info1 = source_table.info1, info2 = source_table.info2, info3 = source_table.info3
FROM source_table
WHERE destination_table.pk1 = source_table.pk1 AND destination_table.pk2 = source_table.pk2;



-- Cleanup
---------------------------

DROP INDEX IF EXISTS source_pk;
DROP TABLE IF EXISTS matches_table;


-- Checks
---------------------------

SELECT COUNT(1) FROM matches_table WHERE is_matching IS FALSE;
SELECT COUNT(1) FROM matches_table WHERE is_matching IS TRUE;
