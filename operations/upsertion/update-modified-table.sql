-- Phases 5 and 6 of Upsertion-Pipeline
--      5) Producing Update-Data table with
--           a) Deduplicated / Preprocessed
--           b) Only-Modified
--         Records
--         -> Join with additional Selection / Filtering
--      6) Updating Bucket with Update-Data
--         -> FULL Batch-Update

-- Settings

SET enable_seqscan = true;
SET enable_seqscan = false;

-- Preparation
--------------------------------------------------

DROP INDEX IF EXISTS source_key;
CREATE UNIQUE INDEX source_key ON source_table (pk1, pk2);

-- Queries
--------------------------------------------------

-- Step 1) Finding Matches
-- Query for testing plan and performance
EXPLAIN
SELECT
       -- Key
       source_table.pk1 AS pk1,
       source_table.pk2 AS pk2,
       -- New Data (Source / Input)
       source_table.info1 AS new_info1,
       source_table.info2 AS new_info2,
       source_table.info3 AS new_info3,
       -- Old Data (Destination / Bucket)
       destination_table.info1 AS old_info1,
       destination_table.info2 AS old_info2,
       destination_table.info3 AS old_info3
FROM source_table
INNER JOIN destination_table ON
    source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2;

-- Output for further pipeline queries
DROP TABLE IF EXISTS update_matched;
CREATE TABLE update_matched AS (
    SELECT
           -- Key
           source_table.pk1 AS pk1,
           source_table.pk2 AS pk2,
           -- New Data (Source / Input)
           source_table.info1 AS new_info1,
           source_table.info2 AS new_info2,
           source_table.info3 AS new_info3,
           -- Old Data (Destination / Bucket)
           destination_table.info1 AS old_info1,
           destination_table.info2 AS old_info2,
           destination_table.info3 AS old_info3
    FROM source_table
    INNER JOIN destination_table ON
        source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
);


-- Step 2) Merging -> Merge-Deduplication Only (Skipped when using Full-Deduplication)
-- Query for testing plan and performance
EXPLAIN
SELECT
       pk1, pk2,
       COALESCE(new_info1, old_info1) AS new_info1,
       COALESCE(new_info2, old_info2) AS new_info2,
       COALESCE(new_info3, old_info3) AS new_info3,
       old_info1, old_info2, old_info3
FROM update_matched;

-- Output for further pipeline queries
DROP TABLE IF EXISTS update_merged;
CREATE TABLE update_merged AS (
    SELECT
           pk1, pk2,
           COALESCE(new_info1, old_info1) AS new_info1,
           COALESCE(new_info2, old_info2) AS new_info2,
           COALESCE(new_info3, old_info3) AS new_info3,
           old_info1, old_info2, old_info3
    FROM update_matched
);


-- Step 3) Filtering out non-modified data
EXPLAIN
SELECT
       pk1, pk2,
       new_info1 AS info1,
       new_info2 AS info2,
       new_info3 AS info3
FROM update_merged
WHERE new_info1 != old_info1
OR new_info2 != old_info2
OR new_info3 != old_info3; -- At least one change is necessary for modification


-- Pipeline
--------------------------------------------------

-- 1) Separate Queries
-- Important: Successfully automatically optimized to be a single operation
EXPLAIN
WITH matched_data AS (
    SELECT
           -- Key
           source_table.pk1 AS pk1,
           source_table.pk2 AS pk2,
           -- New Data (Source / Input)
           source_table.info1 AS new_info1,
           source_table.info2 AS new_info2,
           source_table.info3 AS new_info3,
           -- Old Data (Destination / Bucket)
           destination_table.info1 AS old_info1,
           destination_table.info2 AS old_info2,
           destination_table.info3 AS old_info3
    FROM source_table
    INNER JOIN destination_table ON
        source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
), merged_data AS (
    SELECT
           pk1, pk2,
           COALESCE(new_info1, old_info1) AS new_info1,
           COALESCE(new_info2, old_info2) AS new_info2,
           COALESCE(new_info3, old_info3) AS new_info3,
           old_info1, old_info2, old_info3
    FROM matched_data
)
SELECT
       pk1, pk2,
       new_info1 AS info1,
       new_info2 AS info2,
       new_info3 AS info3
FROM merged_data
WHERE new_info1 != old_info1
OR new_info2 != old_info2
OR new_info3 != old_info3;

-- 2) Single Query
-- Important: Equivalent to the above one (This is how it looks in the simplified way)
-- SUPER-Important: [Value] != Null ALWAYS produces FALSE
EXPLAIN
SELECT
    -- Key
    source_table.pk1 AS pk1,
    source_table.pk2 AS pk2,
    -- Data
    COALESCE(source_table.info1, destination_table.info1) AS info1,
    COALESCE(source_table.info2, destination_table.info2) AS info2,
    COALESCE(source_table.info3, destination_table.info3) AS info3
FROM source_table
INNER JOIN destination_table ON
    source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
WHERE destination_table.info1 != COALESCE(source_table.info1, destination_table.info1)
OR destination_table.info2 != COALESCE(source_table.info2, destination_table.info2)
OR destination_table.info3 != COALESCE(source_table.info3, destination_table.info3);
-- SUPER-IMPORTANT: There can be additional Filters above Join
--    1) Using Index-Scan for Merge-Join
--    2) Filtering out results via Sequential-Scan

-- Decision: Using Second Query for simplicity - Simply needing to omit COALESCE for Full-Strategy

-- Full-Deduplication Query (Allowing to replace with Null-Values)
EXPLAIN
SELECT
    -- Key
    source_table.pk1 AS pk1,
    source_table.pk2 AS pk2,
    -- Data
    COALESCE(source_table.info1, destination_table.info1) AS info1,
    COALESCE(source_table.info2, destination_table.info2) AS info2,
    COALESCE(source_table.info3, destination_table.info3) AS info3
FROM source_table
INNER JOIN destination_table ON
    source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
WHERE (destination_table.info1 != source_table.info1 OR source_table.info1 IS NULL)
OR (destination_table.info2 != source_table.info2 OR source_table.info2 IS NULL)
OR (destination_table.info3 != source_table.info3 OR source_table.info3 IS NULL);

WITH data AS (
    SELECT * FROM source_table LIMIT 1
)
CREATE TEMPORARY TABLE a AS (SELECT * FROM data);