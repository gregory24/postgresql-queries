-- More performant WITHOUT Seq-Scan
SET enable_seqscan = FALSE;

-- Preemptive Optimization
DROP INDEX IF EXISTS source_pk;
CREATE INDEX IF NOT EXISTS source_pk ON source_table USING BTREE (pk1, pk2);

-- Tests: Using 5M in each Table
-- SUPER-IMPORTANT: Running Updates BEFORE Inserts - Less Records in the Destination-Table to work with

-- Total:
--   No Seq-Scan: 50-55s
--   With Seq-Scan: 98s-128s
BEGIN;

-- 3-5s
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

-- UPDATE PHASE

-- 2-5s
CREATE TEMPORARY TABLE update_data ON COMMIT DROP AS (
    SELECT
        -- Key
        source_table.pk1 AS pk1,
        source_table.pk2 AS pk2,
        -- Data
        COALESCE(source_table.info1, destination_table.info1) AS info1,
        COALESCE(source_table.info2, destination_table.info2) AS info2,
        COALESCE(source_table.info3, destination_table.info3) AS info3
    FROM source_table
    INNER JOIN destination_table ON
        source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
    WHERE destination_table.info1 != COALESCE(source_table.info1, destination_table.info1)
    OR destination_table.info2 != COALESCE(source_table.info2, destination_table.info2)
    OR destination_table.info3 != COALESCE(source_table.info3, destination_table.info3)
);

-- 1-2s
CREATE UNIQUE INDEX update_data_pk ON update_data (pk1, pk2);

-- 18-22s
EXPLAIN
UPDATE destination_table
SET info1 = update_data.info1, info2 = update_data.info2, info3 = update_data.info3
FROM update_data
WHERE destination_table.pk1 = update_data.pk1 AND destination_table.pk2 = update_data.pk2;

-- INSERT PHASE

-- 4-8s
CREATE TEMPORARY TABLE insertion_data ON COMMIT DROP AS (
    SELECT pk1, pk2, info1, info2, info3 FROM (
        SELECT
           source_table.*,
           (CASE
               WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN TRUE
               ELSE FALSE
            END) AS is_new
        FROM source_table
        LEFT JOIN destination_table
            ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
    ) AS data_with_match_info
    WHERE is_new IS TRUE
);

-- 10-13s
INSERT INTO destination_table
SELECT pk1, pk2, info1, info2, info3 FROM insertion_data;

-- 0.5s
DROP INDEX source_pk;

COMMIT;