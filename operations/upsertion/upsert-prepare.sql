-- Source and Destination Tables: 5M Records each
--    Source: 0 -> 5,000,000 (All) => NOT Indexed on PK (Neither Regular, Unique nor Primary-Key Index)
--    Destination: 0 -> 10,000,000 (Even) => Uniquely Indexes on PK (Primary-Key Index)

DROP TABLE IF EXISTS destination_table;
DROP TABLE IF EXISTS source_table;

CREATE TABLE destination_table (
    pk1 INTEGER NOT NULL,
    pk2 INTEGER NOT NULL,

    -- IMPORTANT: Allow nullability for Deduplication-tests
    --    -> COALESCE WILL BE SIMPLIFIED / REMOVED WHEN NULLS ARE NOT POSSIBLE
    info1 INTEGER,
    info2 VARCHAR,
    info3 TEXT,

    CONSTRAINT destination_table_pk PRIMARY KEY (pk1, pk2)
);

-- Constraints can be BOTH Included and Excluded
--   -> Excluding to NOT have Primary Key / Indexes
CREATE TABLE source_table (LIKE destination_table EXCLUDING CONSTRAINTS);


-- Populating with Data
---------------------------

TRUNCATE source_table;
INSERT INTO source_table(pk1, pk2, info1, info2, info3)
SELECT
       i, i,
       (CASE i % 5 WHEN 1 THEN i ELSE NULL END), --  For testing merge-deduplication
       (i || '-2'),
       (CASE i % 2 WHEN 1 THEN (i || '-3') ELSE (i || '-new') END) -- For testing modified / not-modified
FROM generate_series(0, 5 * 10 * 1000, 1) AS series(i);

-- Important: Rerun the WHOLE file before EACH test to reset
TRUNCATE destination_table;
INSERT INTO destination_table(pk1, pk2, info1, info2, info3)
SELECT
       i, i,
       (CASE i % 7 WHEN 1 THEN i ELSE NULL END), --  For testing merge-deduplication
       (i || '-2'),
       (i || '-3')
FROM generate_series(0, 10 * 1000, 2) AS series(i);