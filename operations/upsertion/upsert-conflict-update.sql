SET enable_seqscan = TRUE;

BEGIN;

INSERT INTO destination_table
SELECT * FROM source_table
ON CONFLICT ON CONSTRAINT destination_table_pk DO UPDATE
    SET info1 = EXCLUDED.info1, info2 = EXCLUDED.info2, info3 = EXCLUDED.info3;

COMMIT;