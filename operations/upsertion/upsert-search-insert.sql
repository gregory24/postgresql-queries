SET enable_seqscan = FALSE;

BEGIN;

-- ~5s
CREATE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

-- ~30s
UPDATE destination_table
SET info1 = source_table.info1, info2 = source_table.info2, info3 = source_table.info3
FROM source_table
WHERE destination_table.pk1 = source_table.pk1 AND destination_table.pk2 = source_table.pk2;

-- ~25s
INSERT INTO destination_table (pk1, pk2, info1, info2, info3)
SELECT pk1, pk2, info1, info2, info3 FROM (
    SELECT
        source_table.*
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
    WHERE destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL
) AS source;

-- 1s
DROP INDEX source_pk;

COMMIT;

DROP INDEX source_pk;
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);


-- Testing Performance

SET enable_seqscan = FALSE; -- <2s
SET enable_seqscan = TRUE; -- <2s

-- With Index: ~40s
-- Without Index: 4m+
EXPLAIN
UPDATE destination_table
SET info1 = source_table.info1, info2 = source_table.info2, info3 = source_table.info3
FROM source_table
WHERE destination_table.pk1 = source_table.pk1 AND destination_table.pk2 = source_table.pk2;

-- Search
----------------
-- Method 1) Anti-Join (Merge / Nested-Loop / Hash)
-- With Index (On Source) + Seq-Scan Disabled: Merge-Join with Index-Scan / Parallel Index-Scan => 9s
-- Without Index (On Source) + Seq-Scan Enabled: Hash-Join with Seq-Scans => 18s
EXPLAIN
SELECT pk1, pk2, info1, info2, info3 FROM (
    SELECT
        source_table.*
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
    WHERE destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL
) AS source;

-- Method 2) Conditional with Filter-Operation (After Merge-Join) -> NOT using Anti-Join
EXPLAIN
SELECT pk1, pk2, info1, info2, info3 FROM (
    SELECT
        source_table.*,
        (CASE
            WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN TRUE
            ELSE FALSE
        END) AS is_new
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
) AS source
WHERE is_new IS TRUE;

