BEGIN;

SET enable_seqscan = FALSE;

-- 2-3s
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

-- ~4s
CREATE TEMPORARY TABLE matches_table ON COMMIT DROP AS (
    SELECT
       source_table.pk1 AS pk1,
       source_table.pk2 AS pk2,
       (CASE
           WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN FALSE
           ELSE TRUE
        END) AS is_matching
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
);

-- 2-3s
CREATE UNIQUE INDEX matches_table_index ON matches_table USING BTREE (pk1, pk2, is_matching);

--
UPDATE destination_table
SET info1 = source.info1, info2 = source.info2, info3 = source.info3
FROM (
    SELECT pk1, pk2, info1, info2, info3 FROM source_table
    WHERE EXISTS (
        SELECT 1 FROM matches_table
        WHERE matches_table.pk1 = source_table.pk1
        AND matches_table.pk2 = source_table.pk2
        AND matches_table.is_matching IS TRUE
    )
) AS source
WHERE destination_table.pk1 = source.pk1
    AND destination_table.pk2 = source.pk2;

--
INSERT INTO destination_table
SELECT * FROM source_table
WHERE EXISTS (
    SELECT 1 FROM matches_table
    WHERE matches_table.pk1 = source_table.pk1
    AND matches_table.pk2 = source_table.pk2
    AND matches_table.is_matching IS FALSE
);

-- 1-2s
DROP INDEX source_pk;

SET enable_seqscan = TRUE;

COMMIT;

-- Testing Performance

DROP INDEX IF EXISTS source_pk;
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

DROP TABLE IF EXISTS matches_table;

CREATE TABLE matches_table AS (
    SELECT
       source_table.pk1 AS pk1,
       source_table.pk2 AS pk2,
       (CASE
           WHEN destination_table.pk1 IS NULL AND destination_table.pk2 IS NULL THEN FALSE
           ELSE TRUE
        END) AS is_matching
    FROM source_table
    LEFT JOIN destination_table
        ON source_table.pk1 = destination_table.pk1 AND source_table.pk2 = destination_table.pk2
);

DROP INDEX IF EXISTS matches_table_index;
CREATE UNIQUE INDEX matches_table_index ON matches_table USING BTREE (pk1, pk2, is_matching);

-- 1) Insert

SET enable_seqscan = FALSE; -- Merge-Join (Index-Scan, Index-Only-Scan) => 12s
SET enable_seqscan = TRUE; -- Hash-Join (Seq-Scan, Seq-Scan) => 20s

EXPLAIN
INSERT INTO destination_table
SELECT * FROM source_table
WHERE EXISTS (
    SELECT 1 FROM matches_table
    WHERE matches_table.pk1 = source_table.pk1
    AND matches_table.pk2 = source_table.pk2
    AND matches_table.is_matching IS FALSE
);


-- 2) Update

-- Same with Seq-Scan ON / OFF: Nested-Loop-Join(Merge-Join(Index-Scan, Index-Scan), Index-Scan) => 20s

EXPLAIN
UPDATE destination_table
SET info1 = source.info1, info2 = source.info2, info3 = source.info3
FROM (
    SELECT pk1, pk2, info1, info2, info3 FROM source_table
    WHERE EXISTS (
        SELECT 1 FROM matches_table
        WHERE matches_table.pk1 = source_table.pk1
        AND matches_table.pk2 = source_table.pk2
        AND matches_table.is_matching IS TRUE
    )
) AS source
WHERE destination_table.pk1 = source.pk1
    AND destination_table.pk2 = source.pk2;
