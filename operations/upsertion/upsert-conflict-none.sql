BEGIN;

SET enable_seqscan = FALSE;

-- 2-3s
CREATE UNIQUE INDEX source_pk ON source_table USING BTREE (pk1, pk2);

--  Seq-Scan On: Hash-Join (Seq-Scan, Seq-Scan) -> 1m20s (BAD)
--  Seq-Scan Off: Merge-Join (Index-Scan, Index-Scan) -> ~20s (GOOD)
--     => Disable Seq-Scan MANUALLY
UPDATE destination_table
SET info1 = source_table.info1, info2 = source_table.info2, info3 = source_table.info3
FROM source_table
WHERE destination_table.pk1 = source_table.pk1 AND destination_table.pk2 = source_table.pk2;

-- ~30s (DRAGGING DOWN)
INSERT INTO destination_table
SELECT * FROM source_table
ON CONFLICT ON CONSTRAINT destination_table_pk DO NOTHING;

-- ~1s
DROP INDEX source_pk;

SET enable_seqscan = TRUE;

COMMIT;