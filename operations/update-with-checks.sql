------------------------------------------------------------------------------------------------------------------------
-- Tables
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS update_destination;

CREATE TABLE update_destination(
    key INTEGER NOT NULL,
    ts TIMESTAMPTZ NOT NULL,
    info1 TEXT,
    info2 FLOAT,
    sources JSONB NOT NULL
);

DROP TABLE IF EXISTS update_source;

CREATE TABLE update_source (
    LIKE update_destination
);

------------------------------------------------------------------------------------------------------------------------
-- Data
------------------------------------------------------------------------------------------------------------------------

TRUNCATE update_destination;

INSERT INTO update_destination (key, ts, info1, info2, sources)
SELECT
    i,
    (NOW() - (i * interval '1 millisecond') + ((i % 2) * interval '1 hour')),
    i::TEXT,
    i::FLOAT,
    '{}'::JSONB
FROM generate_series(1, 1000 * 1000) AS series(i);

TRUNCATE update_source;

INSERT INTO update_source (key, ts, info1, info2, sources)
SELECT
    i * 2,
    NOW(),
    (CASE i % 2 WHEN 0 THEN i::TEXT ELSE NULL END),
    (CASE i % 3 WHEN 0 THEN i::FLOAT ELSE NULL END),
    JSON_BUILD_OBJECT('source', JSON_BUILD_OBJECT(i, TRUE))
FROM generate_series(1, 1000 * 1000) AS series(i);

------------------------------------------------------------------------------------------------------------------------
-- Indexing
------------------------------------------------------------------------------------------------------------------------

DROP INDEX IF EXISTS update_key_idx;

CREATE UNIQUE INDEX update_key_idx ON update_destination USING BTREE (key);

-----------------------------------------------------------------
-- Query
-----------------------------------------------------------------

SET enable_hashjoin = FALSE;

-- Hash-Join: 8s-10s
-- Merge-Join: 1.5s-2.5s

-- DECISION: NO INDEXING - Receiving almost identical performance
--     -> NEEDLESS / USELESS IF NESTED-LOOP JOIN IS TO BE PERFORMED (Small Inputs <=0.5K)

BEGIN;

-- ~1s
-- Potential optimization to avoid materialization
-- CREATE UNIQUE INDEX tmp_update_key_idx ON update_source USING BTREE (key);

-- With Index: ~600ms
-- Without Index: ~1.5s
--    -> Around the same => NO POINT IN INDEXING
UPDATE update_destination
SET
    -- 1) Data-Modifications
    info1 = COALESCE(update_source.info1, update_destination.info1),
    info2 = COALESCE(update_source.info2, update_destination.info2),
    ts = COALESCE(update_source.ts, update_destination.ts),
    -- 2) Sources-Modification
    sources = JSONB_SET(
        update_destination.sources,
        '{source}',
        COALESCE((update_destination.sources->'source')::JSONB, '{}'::JSONB)
        ||
        (update_source.sources->'source')::JSONB
    )
FROM update_source
WHERE
    -- 1) Match Condition (Joining)
    update_destination.key = update_source.key AND
    -- 2) Timestamp Condition (Latest-Write-Wins)
    update_destination.ts <= update_source.ts AND
    -- 3) Is-Modified
    (
        update_destination.info1 != COALESCE(update_source.info1, update_destination.info1)
        OR
        update_destination.info2 != COALESCE(update_source.info2, update_destination.info2)
    );

-- DROP INDEX tmp_update_key_idx;

COMMIT;
