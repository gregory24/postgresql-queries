-----------------------------------------------------------------------------------------------------------------------
-- Setup
-----------------------------------------------------------------------------------------------------------------------

-- Tables

DROP TABLE status_table;

CREATE TABLE status_table (
    master_id INTEGER NOT NULL,
    synced BOOLEAN NOT NULL,
    metadata JSONB
);

CREATE TABLE records_batch (
    master_id INTEGER NOT NULL,
    data TEXT NOT NULL
);

-- Population

TRUNCATE status_table;
INSERT INTO status_table (master_id, synced)
SELECT i, (CASE i%2 WHEN 1 THEN TRUE ELSE FALSE END) FROM generate_series(0, 1000 * 1000) AS series(i);

TRUNCATE records_batch;
INSERT INTO records_batch (master_id, data)
SELECT master_id, CAST(master_id AS TEXT) FROM status_table TABLESAMPLE bernoulli(0.1);

-- Indexing

-- General - NOT as good as Partial for Existing-Search
--      => NOT USING
DROP INDEX status_table_idx;
CREATE INDEX status_table_idx ON status_table USING BTREE (master_id);

-- Partial-Index: Works better - Simply performing an Index-Scan without needing to perform additional filtering
--    New-Query: Using for Anti-Join with Nested-Loop -> Index-Only-Scan (NOT USING ANY OTHER COLUMNS)
--      => USING
DROP INDEX status_table_idx_synced;
CREATE INDEX status_table_idx_synced ON status_table USING BTREE (master_id) WHERE synced IS TRUE;

-----------------------------------------------------------------------------------------------------------------------
-- Operations
-----------------------------------------------------------------------------------------------------------------------

SET enable_seqscan = FALSE;
SET enable_hashjoin = FALSE;

-- New (BAD - Using Anti-Scan)
EXPLAIN
SELECT * FROM records_batch
WHERE master_id NOT IN (SELECT master_id FROM status_table WHERE synced IS TRUE);

-- New (GOOD - Using PERFORMANT NESTED-LOOP Anti-Join)
EXPLAIN
SELECT records_batch.master_id, records_batch.data FROM records_batch
LEFT JOIN status_table ON records_batch.master_id = status_table.master_id AND synced IS TRUE
WHERE status_table.synced IS NULL;

-- Existing
--    Regular Index: Nested-Loop Join -> Additional Filtering on Synced
--    Partial Index: Nested-Loop Join ONLY
EXPLAIN
SELECT * FROM records_batch
WHERE master_id IN (SELECT master_id FROM status_table WHERE synced IS TRUE);

CREATE TABLE tmp AS (
    SELECT * FROM octolis_temporary."61276f7a-020c-e5dc-2e13-4bc5786137fa"
    UNION ALL
    SELECT * FROM octolis_temporary."61276f7a-020c-e5dc-2e13-4bc5786137fa"
);
