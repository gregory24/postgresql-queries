INSERT INTO contacts(masterid, id_splio)
SELECT gen_random_uuid(), i::TEXT FROM generate_series(0, 1000 * 1000) AS series(i);

ALTER TABLE "61152b93-00e8-5386-f90e-75546a297a4e"
ALTER COLUMN masterid TYPE UUID USING masterid::UUID;

TRUNCATE "61152b93-00e8-5386-f90e-75546a297a4e";
INSERT INTO "61152b93-00e8-5386-f90e-75546a297a4e"(masterid, fidelity_id)
SELECT masterid, '123' AS fidelity_id FROM contacts TABLESAMPLE bernoulli(0.1);

DROP INDEX master_id_index;
CREATE INDEX master_id_index ON contacts USING BTREE(masterid);

SET enable_hashjoin = TRUE;
SET enable_hashjoin = FALSE;

-- Indexed: 65ms - Nested loop

EXPLAIN
UPDATE contacts
SET fidelity_id = fidelity_id_data.fidelity_id
FROM (
    SELECT masterid, fidelity_id FROM "61152b93-00e8-5386-f90e-75546a297a4e"
    WHERE fidelity_id IS NOT NULL
) AS fidelity_id_data
WHERE contacts.masterid = fidelity_id_data.masterid;
