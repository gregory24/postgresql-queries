-- Large table
CREATE TABLE vals AS (
    SELECT ((i % 1000 + i * (i % 100)) % 10000) AS val FROM generate_series(0, 1000 * 1000) AS series(i)
);

-- Heavy operation (3s)
SELECT val FROM vals ORDER BY vals;

-- Cursor for reading output

BEGIN;

DECLARE new_data CURSOR FOR
    SELECT val FROM vals ORDER BY vals;

-- OPEN new_data;

-- 3s
FETCH FORWARD 200 FROM new_data;

 -- 100ms
FETCH FORWARD 200 FROM new_data;

-- 95ms
FETCH FORWARD 1000 FROM new_data;

COMMIT;

-- Outcome: The query is ONLY really ran
