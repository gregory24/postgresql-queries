CREATE TABLE source1 (id INT, val TEXT);
CREATE TABLE source2 (id INT, val TEXT);

INSERT INTO source1(id, val)
SELECT i, i::TEXT FROM generate_series(1, 1000 * 1000) AS s(i);

INSERT INTO source2(id, val)
SELECT i, i::TEXT FROM generate_series(1 + 1000 * 1000, 2 * 1000 * 1000) AS s(i);

DROP TABLE IF EXISTS buffer;

-- Direct Creation
-- Performance: 800ms, 700ms, 750ms -> ~750ms

CREATE TEMPORARY TABLE buffer AS (
    SELECT * FROM source1
    UNION ALL
    SELECT * FROM source2
);

-- Multi-Stage Creation
-- Performance: 900ms, 850ms, 800ms -> ~850ms (MOST EFFICIENT AND FLEXIBLE)

BEGIN;

CREATE TEMPORARY TABLE buffer (id INT, val TEXT);

INSERT INTO buffer(id, val)
SELECT * FROM (
    SELECT * FROM source1
    UNION ALL
    SELECT * FROM source2
) AS all_data;

COMMIT;

-- Multi-Stage Creation (Separate Stages)
-- Performance: 950ms, 900ms, 900ms  -> ~900ms (NOT AS EFFICIENT)

BEGIN;

CREATE TEMPORARY TABLE buffer (id INT, val TEXT);

INSERT INTO buffer(id, val)
SELECT * FROM source1;

INSERT INTO buffer(id, val)
SELECT * FROM source2;

COMMIT;
