------------------------------------------------------------------------------------------------------------------------
-- DATA AND PREPARATION
------------------------------------------------------------------------------------------------------------------------

-- Sources

-- Source 1

DROP TABLE IF EXISTS test_source_1;

CREATE TABLE test_source_1(
    email TEXT NOT NULL,
    phone TEXT NOT NULL,
    details TEXT,
    salary FLOAT,
    extra TEXT,
    creation_time TIMESTAMPTZ NOT NULL,
    update_time TIMESTAMPTZ NOT NULL
);

INSERT INTO test_source_1(email, phone, details, salary, extra, creation_time, update_time) VALUES
('email1', 'phone1', 'D2', 20, '1111', NOW() - interval '10 hour', NOW() - interval '6 hour'),
('email2', 'phone2', 'D1', 30, '2222', NOW() - interval '10 hour', NOW() - interval '1 hour');

-- Source 2

DROP TABLE IF EXISTS test_source_2;

CREATE TABLE test_source_2(
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    phone TEXT NOT NULL,
    details TEXT,
    salary FLOAT,
    extra TEXT,
    first_write TIMESTAMPTZ NOT NULL,
    last_write TIMESTAMPTZ NOT NULL
);

INSERT INTO test_source_2(first_name, last_name, phone, salary, extra, first_write, last_write) VALUES
('first1', 'last1', 'phone1', 50, 'A', NOW() - interval '10 hour', NOW() - interval '4 hour'),
('first2', 'last2', 'phone2', 50, 'B', NOW() - interval '10 hour', NOW() - interval '3 hour'),
('first3', 'last3', 'phone3', 50, 'C', NOW() - interval '10 hour', NOW() - interval '5 hour');

-- Source 3

DROP TABLE IF EXISTS test_source_3;

CREATE TABLE test_source_3(
    email TEXT NOT NULL,
    address TEXT NOT NULL,
    extra TEXT
);

INSERT INTO test_source_3(email, address, extra) VALUES
('email1', 'address1', '-'),
('email4', 'address4', '-');

------------------------------------------------------------------------------------------------------------------------
-- CLEANUP
------------------------------------------------------------------------------------------------------------------------

-- System

TRUNCATE octolis_test_private.source_state_1;
TRUNCATE octolis_test_private.source_state_2;
TRUNCATE octolis_test_private.source_state_3;

TRUNCATE octolis_test_public.test_object;

------------------------------------------------------------------------------------------------------------------------
-- CHECKING
------------------------------------------------------------------------------------------------------------------------

SELECT
    masterid,
    (key::JSONB)->>'email' AS email
FROM (
    SELECT
           masterid,
           sources->'source-1' AS src,
           (jsonb_each(sources->'source-1')).*
    FROM octolis_test_public.test_object
) AS sources_data
WHERE value::BOOLEAN is TRUE;
