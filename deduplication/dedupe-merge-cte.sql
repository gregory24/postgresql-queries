-- Deduplication with MERGE Strategy
-- Merging Columns / Values of Records that are in the same Group together

BEGIN;

-- Optimizing Window-Function (ONLY INITIAL -> DOES NOT IMPROVE ANYTHING IN THE LONG RUN)
CREATE INDEX deduplication_table_index ON deduplication_table USING BTREE (
    -- Partition-Key -> Splitting Records into Groups
    pk1, pk2,

    -- Sort-Key -> Ordering Records within Groups
    sk1 DESC, sk2 DESC
    -- IMPORTANT: Exact same Ordering-Direction for Sort-Key for most efficient performance
);

DROP TABLE IF EXISTS results;

-- Suitable: Using the first available value from the group and combining it with Main-Record
--     Partition-Key: Splits Records into individual Groups fo deduplication
--     Sort-Key: Determines Priority of Records in the Group to find Main-Record for Merging
--     Main-Record: A Record in the Group with the highest priority according to Sort-Key
--     Steps:
--        1) Finding Main-Record and determining Offsets of Columns
--          a) Finding the Main-Record for each Group according to Partition-Key and Sort-Key (First One)
--          b) Specifying Offsets of Columns with Values in their respective Groups if they have values
--              i) Has Value / Not Null: Providing an offset of a Record with Non-Null Column in the Group
--              ii) No Value / Null: Returning NULL to have it ignored when searching for the first value
--        2) Finding Value-Offsets of the first Records in the Group that values for specific Columns
--              -> The first occurrences have the most PRIORITY (Have the smallest Offsets)
--              => Having Offsets assigned to ALL Records of the Group (In order to retrieve it next step)
--              - Requires looking ahead / using following Records (To have MIN assigned to the Main-Record)
--        3) Assigning the first occurrences of values in column to ALL Records in their Groups
--              -> Populating Columns of Records in Groups according to Value-Offsets (First available)
--              => Main-Record uses Column-Values of Records with the highest PRIORITY
--              - Requires looking ahead / using following Records (Accessing next Records in the Group)
--        4) Filtering out all Records that are NOT Main-Records and formatting Main-Records
--              -> Making it look as if their Merged-Columns and their Real-Columns
-- Important: Can use Window-Functions inside of Cases / Conditionals (BOTH as results and conditional-values)
--              -> Offsets / Nulls, Main-Record (Determining based on Row-Number / LAG), etc.
CREATE TABLE results AS (
WITH offsets AS (
    -- No need to specify range - Accessing all the rows until the current one is sufficient enough
    SELECT
        -- ALL the values
        *,

        -- Group-Offset for filtering out the first record
        -- Method 1: Determining position of the record in the Partition / Group (First one -> MAIN)
        --    -> GOOD since it does not rely on nullability
        (CASE ROW_NUMBER() OVER pk_sk_window
        WHEN 1 THEN TRUE
        ELSE FALSE
        END) AS main_record,
        -- Method 2: Checking whether there is a preceding (No Preceding value -> Record is the first -> MAIN)
        --    -> NOT GOOD ENOUGH (There's no awy to know which column is NON-NULLABLE for checking)
        (CASE
        WHEN LAG(id, 1) OVER pk_sk_window IS NULL THEN TRUE
        ELSE FALSE
        END) AS main_record_2,


        -- Offsets of Rows / Records that have values for specific columns
        --    NULL - No value
        --    Offset - Has value / Position according to Sorting-Key (Priority)
        --  -> Each Record that has a value in a Column has an Offset for that Column
        --  => Offsets are used for accessing values of Columns in order to use them instead of NULLs
        --  - Allowing Main-Record to use specific Values of Records instead of it's own NULL-Values
        (CASE
        WHEN v1 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v1_offset,
        (CASE
        WHEN v2 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v2_offset,
        (CASE
        WHEN v3 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v3_offset
    FROM deduplication_table
    WINDOW pk_sk_window AS (PARTITION BY pk1, pk2 ORDER BY sk1 DESC, sk2 DESC)
), value_offsets AS (
    -- Specifying RANGE in order to be able to find the minimum row from the ENTIRE group
    --    -> The first row with value in a specific column is accessible from ANY row (Even the first one)
    --    => Can filter out the first row directly from the group at the next step
    SELECT
        *,
        -- Taking the first OFFSET for value that is NOT NULL -> NULLs are skipped automatically
        --   -> Providing the first Offset of Record that has a Non-Null value for a specific Column
        --   => Same FOR ALL Records no matter their position (Includes Main-Record)
        MIN(v1_offset) OVER pk_sk_window AS v1_value_offset,
        MIN(v2_offset) OVER pk_sk_window AS v2_value_offset,
        MIN(v3_offset) OVER pk_sk_window AS v3_value_offset
    FROM offsets
    WINDOW pk_sk_window AS (
        PARTITION BY pk1, pk2
        ORDER BY sk1 DESC, sk2 DESC
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
), merged_values AS (
    -- Specifying RANGE in order to be able to access ANY row (Even the FOLLOWING ONES)
    --    -> By default NTH only allows accessing PRECEDING rows
    --    => Populating / Merging Main-Record with Records that have first Offsets for specific Columns
    --    - Offset determines their priority (Using values of Records with highest performance)
    SELECT
        *,
        NTH_VALUE(v1, CAST(v1_value_offset AS INTEGER)) OVER pk_sk_window AS v1_value,
        NTH_VALUE(v2, CAST(v2_value_offset AS INTEGER)) OVER pk_sk_window AS v2_value,
        NTH_VALUE(v3, CAST(v3_value_offset AS INTEGER)) OVER pk_sk_window AS v3_value
    FROM value_offsets
    WINDOW pk_sk_window AS (
        PARTITION BY pk1, pk2
        ORDER BY sk1 DESC, sk2 DESC
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
    -- CANNOT use it here since Following / Value / Merge rows are going to be filtered out from the Window
    -- WHERE group_offset = 1 -- Taking the first row with merged values
)
SELECT
    id, pk1, pk2, sk1, sk2,
    v1_value AS v1,
    v2_value AS v2,
    v3_value AS v3
FROM merged_values
WHERE main_record_2 = TRUE -- Only taking the main record that got other records merged with it
);

DROP INDEX IF EXISTS deduplication_table_index;

COMMIT;


-- Testing Performance
------------------------------

SET enable_seqscan = FALSE;
SET enable_seqscan = TRUE;

EXPLAIN
WITH offsets AS (
    -- No need to specify range - Accessing all the rows until the current one is sufficient enough
    SELECT
        -- ALL the values
        *,

        -- Group-Offset for filtering out the first record
        -- Method 1: Determining position of the record in the Partition / Group (First one -> MAIN)
        --    -> GOOD since it does not rely on nullability
        (CASE ROW_NUMBER() OVER pk_sk_window
        WHEN 1 THEN TRUE
        ELSE FALSE
        END) AS main_record,
        -- Method 2: Checking whether there is a preceding (No Preceding value -> Record is the first -> MAIN)
        --    -> NOT GOOD ENOUGH (There's no awy to know which column is NON-NULLABLE for checking)
        (CASE
        WHEN LAG(id, 1) OVER pk_sk_window IS NULL THEN TRUE
        ELSE FALSE
        END) AS main_record_2,


        -- Offsets of Rows / Records that have values for specific columns
        --    NULL - No value
        --    Offset - Has value / Position according to Sorting-Key (Priority)
        --  -> Each Record that has a value in a Column has an Offset for that Column
        --  => Offsets are used for accessing values of Columns in order to use them instead of NULLs
        --  - Allowing Main-Record to use specific Values of Records instead of it's own NULL-Values
        (CASE
        WHEN v1 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v1_offset,
        (CASE
        WHEN v2 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v2_offset,
        (CASE
        WHEN v3 IS NULL THEN NULL
        ELSE ROW_NUMBER() OVER pk_sk_window
        END) AS v3_offset
    FROM deduplication_table
    WINDOW pk_sk_window AS (PARTITION BY pk1, pk2 ORDER BY sk1 DESC, sk2 DESC)
), value_offsets AS (
    -- Specifying RANGE in order to be able to find the minimum row from the ENTIRE group
    --    -> The first row with value in a specific column is accessible from ANY row (Even the first one)
    --    => Can filter out the first row directly from the group at the next step
    SELECT
        *,
        -- Taking the first OFFSET for value that is NOT NULL -> NULLs are skipped automatically
        --   -> Providing the first Offset of Record that has a Non-Null value for a specific Column
        --   => Same FOR ALL Records no matter their position (Includes Main-Record)
        MIN(v1_offset) OVER pk_sk_window AS v1_value_offset,
        MIN(v2_offset) OVER pk_sk_window AS v2_value_offset,
        MIN(v3_offset) OVER pk_sk_window AS v3_value_offset
    FROM offsets
    WINDOW pk_sk_window AS (
        PARTITION BY pk1, pk2
        ORDER BY sk1 DESC, sk2 DESC
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
), merged_values AS (
    -- Specifying RANGE in order to be able to access ANY row (Even the FOLLOWING ONES)
    --    -> By default NTH only allows accessing PRECEDING rows
    --    => Populating / Merging Main-Record with Records that have first Offsets for specific Columns
    --    - Offset determines their priority (Using values of Records with highest performance)
    SELECT
        *,
        NTH_VALUE(v1, CAST(v1_value_offset AS INTEGER)) OVER pk_sk_window AS v1_value,
        NTH_VALUE(v2, CAST(v2_value_offset AS INTEGER)) OVER pk_sk_window AS v2_value,
        NTH_VALUE(v3, CAST(v3_value_offset AS INTEGER)) OVER pk_sk_window AS v3_value,
        JSONB_OBJECT_AGG(id, TRUE) OVER pk_sk_window AS source_ids
    FROM value_offsets
    WINDOW pk_sk_window AS (
        PARTITION BY pk1, pk2
        ORDER BY sk1 DESC, sk2 DESC
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
    -- CANNOT use it here since Following / Value / Merge rows are going to be filtered out from the Window
    -- WHERE group_offset = 1 -- Taking the first row with merged values
)
SELECT
    id, pk1, pk2, sk1, sk2,
    v1_value AS v1,
    v2_value AS v2,
    v3_value AS v3,
    JSON_BUILD_OBJECT('source-1', source_ids) AS source_ids
FROM merged_values
WHERE main_record_2 IS TRUE;

