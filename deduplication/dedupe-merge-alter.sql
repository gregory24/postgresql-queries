-- NOT COMPLIANT WITH SQL-STANDARD - DOES NOT ALLOW IGNORING NULLS
--   -> DOES NOT WORK
WITH merged AS (
    SELECT
        id, pk1, pk2, sk1, sk2,
        (CASE ROW_NUMBER() OVER (PARTITION BY pk1, pk2 ORDER BY sk1 DESC, sk2 DESC)
        WHEN 1 THEN TRUE
        ELSE FALSE
        END) AS main_record,
        FIRST_VALUE(v1) OVER pk_sk_window AS v1,
        FIRST_VALUE(v2) OVER pk_sk_window AS v2,
        FIRST_VALUE(v3) OVER pk_sk_window AS v3
    FROM deduplication_table
    WINDOW pk_sk_window AS (
        PARTITION BY pk1, pk2 ORDER BY sk1 DESC, sk2 DESC
        -- Finding first Non-Null from current row till the end of Partition
        -- Default: Finding first Non-Null from the beginning till the current Row
        RANGE BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING
    )
)
SELECT id, pk1, pk2, sk1, sk2, v1, v2, v3 FROM merged
WHERE main_record IS TRUE;