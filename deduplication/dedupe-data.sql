DROP TABLE IF EXISTS deduplication_table;

CREATE TABLE deduplication_table (
    -- Identification
    id INTEGER NOT NULL,

    -- Partition-Key
    pk1 INTEGER NOT NULL,
    pk2 INTEGER NOT NULL,

    -- Sort-Key
    sk1 INTEGER NOT NULL,
    sk2 INTEGER NOT NULL,

    -- Values
    v1 INTEGER,
    v2 INTEGER,
    v3 INTEGER
);


-- Sample-Data
TRUNCATE deduplication_table;

INSERT INTO deduplication_table(id, pk1, pk2, sk1, sk2, v1, v2, v3) VALUES
    (1, 1, 2, 1, 1, 1, 1, 1),
    (2, 1, 2, 1, 2, NULL, 2, 2),
    (3, 1, 2, 2, 1, NULL, 3, NULL),
    (4, 2, 2, 1, 1, NULL, NULL, NULL),
    (5, 2, 2, 0, 0, 5, 5, 5);
-- Results:
--    Full: (3, 1, 2, 2, 1, NULL, 3, NULL), (4, 2, 2, 1, 1, NULL, NULL, NULL)
--    Merge: (3, 1, 2, 2, 1, 1, 3, 2), (4, 2, 2, 1, 1, 5, 5, 5)


TRUNCATE deduplication_table;
INSERT INTO deduplication_table(id, pk1, pk2, sk1, sk2, v1, v2, v3)
SELECT
    i AS id,

    CAST(FLOOR(RANDOM() * 99 + 1) AS INTEGER) AS pk1,
    CAST(FLOOR(RANDOM() * 99 + 1) AS INTEGER) AS pk2,

    CAST(FLOOR(RANDOM() * 100) AS INTEGER) AS sk1,
    CAST(FLOOR(RANDOM() * 100) AS INTEGER) AS sk2,

    (CASE
        WHEN RANDOM() > 0.3 THEN NULL
        ELSE i
    END) AS v1,
    (CASE
        WHEN RANDOM() > 0.5 THEN NULL
        ELSE i
    END) AS v2,
    (CASE
        WHEN RANDOM() > 0.7 THEN NULL
        ELSE i
    END) AS v3
FROM generate_series(0, 1000) AS series(i);

-- Optimizations and Performance
CREATE INDEX deduplication_table_index ON deduplication_table USING BTREE (
    -- Partition-Key -> Splitting Records into Groups
    pk1, pk2,

    -- Sort-Key -> Ordering Records within Groups
    sk1 DESC, sk2 DESC
    -- IMPORTANT: Exact same Ordering-Direction for Sort-Key for most efficient performance
);

-- Speeding up Insertion
DROP INDEX IF EXISTS deduplication_table_index;

