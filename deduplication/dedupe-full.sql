-- Deduplication with FULL Strategy
-- Taking full Records with highest Priority from their respective Groups
--    Partition-Key: Distinct-On (ALL COLUMNS) -> Splitting Records into Groups (Primary)
--    Sort-Key: Ordering AFTER Partition-Key -> Ordering Records within Groups (Secondary)
-- Important: BOTH Partition-Key and Sort-Key are used for Sorting first -> Using respective operations
--    - Same as with Window-Aggregation

BEGIN;

-- Optimizations and Performance
-- Important: Deduplication-Table should be optimized by a SINGLE Index
--    -> A single Sort-Operation is for BOTH 1) Unique (Distinction) 2) Window-Aggregation (Window)
CREATE INDEX deduplication_table_index ON deduplication_table USING BTREE (
    pk1, pk2,
    sk1 DESC, sk2 DESC
);

DROP TABLE IF EXISTS results;

CREATE TABLE results AS (
    SELECT DISTINCT ON (pk1, pk2) * FROM deduplication_table
    ORDER BY pk1, pk2, sk1 DESC, sk2 DESC
);

DROP INDEX IF EXISTS deduplication_table_index;

COMMIT;

-- Testing Performance
-----------------------------------
EXPLAIN ANALYZE
SELECT
    DISTINCT ON (pk1, pk2) *,
    JSON_BUILD_OBJECT('source-1', JSON_BUILD_OBJECT(id, TRUE))
FROM deduplication_table
ORDER BY pk1, pk2, sk1 DESC, sk2 DESC;

SET enable_seqscan = TRUE;
SET enable_seqscan = FALSE;

-- Performance:
--   With Index: ~15s
--   Without Index: ~30s