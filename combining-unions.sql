CREATE TABLE target_table(
    id INTEGER,
    val INTEGER,
    info TEXT
);

CREATE TABLE input_table(
    sid INTEGER,
    val INTEGER,
    info TEXT
);

INSERT INTO target_table VALUES (1, 10, 'a'), (2, 20, 'b'), (3, 30, 'a'), (4, 40, 'b');

INSERT INTO input_table VALUES (5, 10, 'c'), (6, 20, 'b');

-- Multiple Unions with Distinctions
SELECT val, info FROM (
    SELECT DISTINCT ON (id) * FROM (
        SELECT * FROM target_table WHERE info = 'a'
        UNION ALL
        SELECT * FROM target_table WHERE val >= 20
    ) AS __target_candidates__
    UNION ALL
    SELECT * FROM input_table
) AS __all_candidates__;
