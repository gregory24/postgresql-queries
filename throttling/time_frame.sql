DROP TABLE throttling_table_frames;

CREATE TABLE throttling_table_frames
(
    record_id SERIAL PRIMARY KEY,
    data TEXT,
    at_time TIMESTAMPTZ,

    resource_id VARCHAR(255) NOT NULL,
    time_frame INTEGER NOT NULL, -- Math.floor(Date.now() / [size-of-the-time-frame])

    CONSTRAINT throttler_frames UNIQUE (resource_id, time_frame)
);

-- Note: Allowing milliseconds?
-- Get current timestamp without milliseconds
SELECT DATE_TRUNC('seconds', NOW());

-- Get timestamp of seconds (UNIX)
SELECT EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW()));

-- Get time-frame (10 seconds)
SELECT FLOOR(EXTRACT(EPOCH FROM NOW()) / 10);

-- Get time until the next time-frame (10 seconds)
SELECT 10 - CAST(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) AS INTEGER) % 10;

-- Get time-frame with an offset (10 seconds window and 5 second offset)
-- Does NOT affect the time-frame, the next time-frame simply comes sooner
SELECT FLOOR((EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) + 5) / 10);

-- Get the following time-frame (The one ahead of the current one)
SELECT FLOOR((EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) + (
    10 - CAST(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) AS INTEGER) % 10
)) / 10);

-- Insert with throttling 5 seconds (Taking the first entry -> Doing nothing)
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
VALUES (
        CAST(NOW() AS TEXT),
        NOW(),
        'resource',
        FLOOR(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) / 5)
)
ON CONFLICT ON CONSTRAINT throttler_frames DO NOTHING
RETURNING *;

-- Insert with throttling 5 seconds (Taking the last entry -> Updating time and data)
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
VALUES (
        CAST(NOW() AS TEXT),
        NOW(),
        'resource',
        FLOOR(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) / 5)
)
ON CONFLICT ON CONSTRAINT throttler_frames DO UPDATE SET
    data = EXCLUDED.data,
    at_time = EXCLUDED.at_time
RETURNING *;


-- Performance test (Insert into table with 5M)
-- Time-Window: 5s
-----------------------------------------------

-- Population
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
SELECT
       CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       CAST((i % 10) AS TEXT),
       i * 100
FROM generate_series(0, 5 * 1000 * 1000) AS s(i);

-- Test: Throttle with the Last
--      With Returning: 300ms
--      Without Returning: 30ms
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
VALUES (
        CAST(NOW() AS TEXT),
        NOW(),
        'resource',
        FLOOR(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) / 5)
)
ON CONFLICT ON CONSTRAINT throttler_frames DO UPDATE SET
    data = EXCLUDED.data,
    at_time = EXCLUDED.at_time;

-- Test: Throttle with the First
--      With Returning: 300ms
--      Without Returning: 30ms
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
VALUES (
        CAST(NOW() AS TEXT),
        NOW(),
        'resource',
        FLOOR(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) / 5)
)
ON CONFLICT ON CONSTRAINT throttler_frames DO NOTHING;


--------------------------------------
-- Benchmarking
--------------------------------------

-- Options

SET enable_seqscan = FALSE;
SET enable_seqscan = TRUE;

-- Input
--------------------------------------
CREATE TABLE frames_input_data(
    data TEXT,
    at_time TIMESTAMPTZ,
    resource_id VARCHAR(255) NOT NULL,
    -- Simulating time-frame by simply presenting a number
    time_frame INTEGER NOT NULL
);

TRUNCATE frames_input_data;

INSERT INTO frames_input_data (data, at_time, resource_id, time_frame)
SELECT * FROM (
    -- New-Records to Insert
    SELECT
       'data' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST((i % 500) AS TEXT),
       i
    FROM generate_series(900 * 1000, 1000 * 1000, 2000) AS series(i)
    UNION ALL
    -- Existing-Records to Update
    SELECT
        'data-new',
        NOW(),
        resource_id,
        time_frame
    FROM throttling_table_frames TABLESAMPLE BERNOULLI(0.00035)
) AS combined_data;

-- Clearing input records
DELETE FROM throttling_table_frames
WHERE data ILIKE 'data%'; -- TAKES A LOT OF TIME

SELECT COUNT(1) FROM frames_input_data;


-- Data and Population
--------------------------------------

TRUNCATE throttling_table_frames;

-- Old Time-Frames
INSERT INTO throttling_table_frames (data, at_time, resource_id, time_frame)
SELECT
       'initial' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST((i % 500) AS TEXT),
       i
FROM generate_series(1, 20 * 1000 * 1000) AS series(i);


-- Querying
--------------------------------------

-- First Strategy

EXPLAIN (VERBOSE)
WITH new_data AS (
    SELECT data, at_time, resource_id, time_frame AS time_frame FROM frames_input_data
)
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
SELECT * FROM new_data
ON CONFLICT ON CONSTRAINT throttler_frames DO NOTHING;

-- Last Strategy

EXPLAIN (VERBOSE)
WITH new_data AS (
    SELECT data, at_time, resource_id, time_frame AS time_frame FROM frames_input_data
)
INSERT INTO throttling_table_frames(data, at_time, resource_id, time_frame)
SELECT * FROM new_data
ON CONFLICT ON CONSTRAINT throttler_frames DO UPDATE SET
    data = EXCLUDED.data,
    at_time = EXCLUDED.at_time;

CREATE INDEX time_tester ON throttling_table_frames (at_time);
