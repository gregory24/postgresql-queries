DROP TABLE IF EXISTS throttler;

-- Data Model
------------------------------------------------

CREATE TABLE throttler (
    -------------------------
    -- Record information
    -------------------------
    -- Identifier (NOT A PK) -> Already included into another Index
    id BIGSERIAL,

    -- Types: Leading (0), Trailing (1) and Leading-Trailing (2)
    type INTEGER NOT NULL,

    -- Statuses: Waiting-Leading (0), Waiting-Trailing (1), Processing (2), Done (3) and Failed (4)
    status INTEGER NOT NULL,

    -------------------------
    -- Payload and data
    -------------------------

    -- Leading and Trailing payloads
    leading_payload JSON, -- Could be NULL (Trailing Strategy)
    trailing_payload JSON, -- Could be NULL (Leading and Leading-Trailing Strategies)

    -- Timestamps
    leading_produced_at TIMESTAMPTZ,
    leading_consumed_at TIMESTAMP,
    trailing_produced_at TIMESTAMPTZ,
    trailing_consumed_at TIMESTAMP,

    -- Completion Flags (Determining state for processing)
    is_leading_pending BOOLEAN DEFAULT FALSE,
    is_trailing_pending BOOLEAN DEFAULT FALSE,

    -------------------------
    -- Processing information
    -------------------------

    locked_by UUID,
    locked_until TIMESTAMPTZ,

    -------------------------
    -- Throttling information
    -------------------------

    -- Resource / Throttling-Key - Could be Integer, String or UUID
    resource INTEGER NOT NULL,

    -- Time-Frame
    time_frame_size BIGINT NOT NULL, -- Size in seconds
    time_frame_number INTEGER NOT NULL, -- Sequential number of a time-frame according to it's size
    time_frame_start TIMESTAMPTZ NOT NULL, -- Start of the time-frame
    time_frame_end TIMESTAMPTZ NOT NULL, -- End of the time-frame

    -- Unique Constraint
    CONSTRAINT time_frame_throttling UNIQUE (resource, time_frame_number)
);

-- Indexes
------------------------------------------------

-- Index 1: Upsert Index (Unique Constraint with "time_frame_throttling")

-- Index 2: Search Index (Full-Index Scans for BOTH Leading and Trailing conditions)
-- Use-Cases:
--     1) Fetching - Full-Index-Scans on Sub-Queries for Nested-Loop Join with Update (With Hashing and Ordering)
--     2) Deletion - Full-Index-Scans for Nested-Loop Join with Deletion
DROP INDEX IF EXISTS throttler_search_index;
CREATE INDEX throttler_search_index ON throttler USING BTREE (id, status, time_frame_end);

-- Index 3: Update Index (Matching against Values via Index Scan)
DROP INDEX IF EXISTS throttler_update_index;
CREATE INDEX throttler_update_index ON throttler USING BTREE (id, locked_by, status);

-- Population
------------------------------------------------
TRUNCATE throttler;
INSERT INTO throttler (type, status, resource, time_frame_size, time_frame_number, time_frame_start, time_frame_end)
SELECT i % 3, i % 5, i % 10, i % 1000, i, NOW(), NOW() + (interval '1 second' * (i % 1000))
FROM generate_series(1, 1000 * 1000) AS series(i);
