-- Get UNIX Timestamps
--------------------------------------
-- Get Milliseconds 1970 (UNIX) - Using Decimal Point for Milliseconds and Microseconds
SELECT EXTRACT(EPOCH FROM NOW());

-- Rounding to seconds (With Extraction)
SELECT EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW()));

-- Rounding to seconds (With Operation)
SELECT FLOOR(EXTRACT(EPOCH FROM NOW()));

-- Getting a number of time-frame of a specific size (5s)
SELECT FLOOR(EXTRACT(EPOCH FROM DATE_TRUNC('seconds', NOW())) / 5);

-- Get # of a Time-Frame (Serial Number)
--------------------------------------
-- Second-Based (10 second window)
SELECT FLOOR(EXTRACT(EPOCH FROM NOW()) / 10);

-- Millisecond-Based (DOES NOT WORK)
SELECT FLOOR(EXTRACT(EPOCH FROM NOW()) / 10 * 1000);

-- Get Start and End of a Time-Frame according to it's Serial Number
--------------------------------------
-- Second-Based Start and End
SELECT
    TO_TIMESTAMP(frame_number * frame_size) AS frame_start,
    TO_TIMESTAMP((frame_number + 1) * frame_size) AS frame_end
FROM (VALUES (10, FLOOR(EXTRACT(EPOCH FROM NOW()) / 10))) AS frames(frame_size, frame_number);

-- Millisecond-Based Start and End (DO NOT WORK)
SELECT
    frame_number,
    TO_TIMESTAMP(frame_number * frame_size_in_seconds / 1000) AS frame_start,
    TO_TIMESTAMP((frame_number + 1) * frame_size_in_seconds / 1000) AS frame_end
FROM (VALUES (10, FLOOR(EXTRACT(EPOCH FROM NOW()) / 10 * 1000))) AS frames(frame_size_in_seconds, frame_number);

-- IMPORTANT: When Rounding up to Hours / Minutes / Seconds the rest is lost due to FLOORING
--   -> Does NOT matter whether milliseconds are present
