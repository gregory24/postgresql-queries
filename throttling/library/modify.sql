-- Used for Switching Waiting-Tailing, Completing, Failing and Retrying
-- Plan: Nested-Loop Update
EXPLAIN
UPDATE throttler
SET
    status = modification_data.status,
    leading_consumed_at = COALESCE(modification_data.leading_consumed_at, throttler.leading_consumed_at),
    is_leading_pending = modification_data.is_leading_pending,
    trailing_consumed_at = modification_data.trailing_consumed_at,
    is_trailing_pending = modification_data.is_trailing_pending,
    locked_by = NULL,
    locked_until = NULL
FROM (
     SELECT * FROM (VALUES
        -- Leading Completed -> Setting "Done" status and removing Leading and Trailing Flag (pointless)
        (1, 3, NOW(), FALSE, NULL, FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Trailing Failed -> Setting "Failed" status and removing Leading (same thing) and Trailing Flag
        (2, 4, NULL, FALSE, NOW(), FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Leading-Trailing Completed (Leading Processed) -> Setting "Waiting-Trailing" status and removing Leading Flag
        (3, 1, NOW(), FALSE, NULL, FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Leading-Trailing Completed (Trailing Processed) -> Setting "Done" status and removing Leading Flag
        (4, 3, TO_TIMESTAMP(500), FALSE, NOW(), FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Leading-Trailing Completed (BOTH Processed) -> Setting "Done" status and removing Flags
        (5, 3, NOW(), FALSE, NOW(), FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Leading-Trailing Failed (Leading Processed) -> Setting "Waiting-Trailing" status and removing Leading Flag
        (6, 1, NULL, FALSE, NULL, FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID)),
        -- Leading-Trailing Failed (Trailing Processed) ->
        (7, 4, NULL, FALSE, NULL, FALSE, CAST('d3347a93-0dc4-4820-9ae9-325a818cad66' AS UUID))
     ) AS modification_information (
        id,
        status,
        leading_consumed_at,
        is_leading_pending,
        trailing_consumed_at,
        is_trailing_pending,
        locked_by
    )
) AS modification_data
WHERE throttler.id = modification_data.id
AND throttler.locked_by = modification_data.locked_by
AND throttler.status = 2; -- Being Processed

-- Additional:
--  Retrying: NOT Removing Flag + Performing Incrementing of Failure-Counter for specific Payload(s)
