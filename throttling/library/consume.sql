-- Finding Time-Frames with available records

-- Optimizations
------------------------------------------------

SET enable_seqscan = TRUE;
SET enable_seqscan = FALSE;

-- Index of JUST Filtering -> NOT good enough
DROP INDEX IF EXISTS throttler_consume_index;
CREATE INDEX throttler_consume_index ON throttler USING BTREE (id, status, time_frame_end);

-- Filtering and Sorting Index -> Full-Index Scans with Sorting and Filtering when
--   => Extremely effective with Limiting and Sorting inner queries
DROP INDEX IF EXISTS throttler_consume_index;
CREATE INDEX throttler_consume_index ON throttler USING BTREE (id, status, time_frame_end);

-- Queries
------------------------------------------------

-- Case 1: Leading Payload
SELECT id FROM throttler WHERE status = 0;

-- Case 2: Trailing Payload
SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW();

-- Combining all with sorting and limiting (Fetching the oldest)

-- Combination 1: Sorting and Limiting explicitly
-- Plan: Parallel Bitmap-Scans with manual Sorting -> 326129 / 10s
EXPLAIN
SELECT id FROM (
    SELECT id FROM throttler WHERE status = 0
    UNION ALL
    SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW()
) AS matches
ORDER BY id
LIMIT 100;

-- Combination 2: Limiting and Union that uses Grouping with Sorting
-- Plan: Bitmap-Scans with Sorting-Aggregate -> 475125 / 2s
EXPLAIN
SELECT id FROM (
    SELECT id FROM throttler WHERE status = 0
    UNION
    SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW()
) AS matches
LIMIT 100;

-- Combination 3: Selecting Distinct IDs to rely on Sorting
-- Plan: Bitmap-Scans with Hash-Aggregate =>
EXPLAIN
SELECT DISTINCT id FROM (
    SELECT id FROM throttler WHERE status = 0
    UNION ALL
    SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW()
) AS matches
LIMIT 100;

-- Combination 4: Using OR
-- Plan: Index-Scan using PK -> Manual Filtering based on specified conditions => 13 / 200ms / Unreliable
EXPLAIN
SELECT id FROM throttler
WHERE (status = 0) OR (status = 1 AND time_frame_end <= NOW())
ORDER BY id
LIMIT 100;

-- Combination 5: Union All with Ordering and Limiting within Sub-Queries (THE WAY TO GO)
-- Plan: Full-Index Scans -> Merge-Append with a Sort-Key (Merging results into a sorted output right away)
--    => 18 / 150ms
EXPLAIN
SELECT id FROM (
    (SELECT id FROM throttler WHERE status = 0 ORDER BY id LIMIT 100)
    UNION ALL
    (SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW() ORDER BY id LIMIT 100)
) AS matches
ORDER BY id
LIMIT 100;

-- Query with Locking
------------------------------------------------

-- Method 1: Locking outer-level => 1735 / 200ms
EXPLAIN
SELECT id FROM throttler
WHERE id IN (
    (SELECT id FROM throttler WHERE status = 0 ORDER BY id LIMIT 100)
    UNION ALL
    (SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW() ORDER BY id LIMIT 100)
)
ORDER BY id
LIMIT 100
FOR UPDATE SKIP LOCKED;

-- Method 2: Locking inner-level (NOT ALLOWED, makes sense)
EXPLAIN
SELECT id FROM (
    (SELECT id FROM throttler WHERE status = 0 ORDER BY id LIMIT 100 FOR UPDATE SKIP LOCKED)
    UNION ALL
    (SELECT id FROM throttler WHERE status = 1 AND time_frame_end <= NOW() ORDER BY id LIMIT 100 FOR UPDATE SKIP LOCKED)
) AS matches
ORDER BY id
LIMIT 100;

-- Operation
------------------------------------------------
-- Extremely effective: Using Index-Scans everywhere

-- IMPORTANT: TIME FRAME HAS PASSED (NOT OPEN) -> EXPLICIT (<)

EXPLAIN
UPDATE throttler
SET status = 2, locked_by = CAST('223ed037-5672-46a0-879e-a97bf9d7b2ed' AS UUID), locked_until = NOW()
WHERE id IN (
    SELECT id FROM throttler
    WHERE id IN (
        (SELECT id FROM throttler WHERE status = 0 ORDER BY id LIMIT 100)
        UNION ALL
        (SELECT id FROM throttler WHERE status = 1 AND time_frame_end < NOW() ORDER BY id LIMIT 100)
    )
    ORDER BY id
    LIMIT 100
    FOR UPDATE SKIP LOCKED
);
