INSERT INTO throttler (
    type,
    status,
    leading_payload,
    leading_produced_at,
    is_leading_pending,
    trailing_payload,
    trailing_produced_at,
    is_trailing_pending,
    resource,
    time_frame_size,
    time_frame_number,
    time_frame_start,
    time_frame_end
)
SELECT
    *,
    TO_TIMESTAMP(time_frame_number * time_frame_size / 1000),
    TO_TIMESTAMP((time_frame_number + 1) * time_frame_size / 1000)
FROM (VALUES
    -- Leading Strategy
    (
        0, -- Leading
        0, -- Waiting-Leading
        -- Leading data
        CAST('{"type": "leading", "data": "Leading-Data"}' AS JSON),
        NOW(),
        TRUE,
        -- Trailing data
        CAST(NULL AS JSON),
        CAST(NULL AS TIMESTAMPTZ),
        FALSE,
        -- Throttling data
        1, -- Resource #1
        60 * 60 * 1000, -- 1 hour
        FLOOR(EXTRACT(EPOCH FROM NOW()) * 1000 / (60 * 60 * 1000))
    ),
    -- Trailing Strategy
    (
        1, -- Trailing
        1, -- Waiting-Trailing
        -- Leading data
        CAST(NULL AS JSON),
        CAST(NULL AS TIMESTAMPTZ),
        FALSE,
        -- Trailing data
        CAST('{"type": "trailing", "data": "Trailing-Data"}' AS JSON),
        NOW(),
        TRUE,
        -- Throttling data
        2, -- Resource #2
        2 * 60 * 60 * 1000, -- 2 hours
        FLOOR(EXTRACT(EPOCH FROM NOW()) * 1000 / (2 * 60 * 60 * 1000))
    ),
    -- Leading-Trailing Strategy
    (
        2, -- Leading-Trailing
        0, -- Waiting-Leading
        -- Leading data
        CAST('{"type": "leading-trailing", "data": "Leading-Trailing-Data"}' AS JSON),
        NOW(),
        TRUE,
        -- Trailing data
        CAST(NULL AS JSON),
        CAST(NULL AS TIMESTAMPTZ),
        FALSE,
        -- Throttling data
        3, -- Resource #3
        3 * 60 * 60 * 1000, -- 3 hours
        FLOOR(EXTRACT(EPOCH FROM NOW()) * 1000 / (3 * 60 * 60 * 1000))
    )
) AS input_data(
    type,
    status,
    leading_payload,
    leading_produced_at,
    is_leading_pending,
    trailing_payload,
    trailing_produced_at,
    is_trailing_pending,
    resource,
    time_frame_size,
    time_frame_number
)
ON CONFLICT ON CONSTRAINT time_frame_throttling DO UPDATE SET
    -- Rewriting trailing-task
    trailing_payload = COALESCE(EXCLUDED.leading_payload, EXCLUDED.trailing_payload),
    trailing_produced_at = NOW(),
    is_trailing_pending = TRUE; -- Populating with data regardless


INSERT INTO throttler (
    type,
    status,
    leading_payload,
    leading_produced_at,
    is_leading_pending,
    trailing_payload,
    trailing_produced_at,
    is_trailing_pending,
    resource,
    time_frame_size,
    time_frame_number,
    time_frame_start,
    time_frame_end
)
SELECT
    *,
    TO_TIMESTAMP(time_frame_number * time_frame_size / 1000),
    TO_TIMESTAMP((time_frame_number + 1) * time_frame_size / 1000)
FROM (VALUES
    -- Leading Strategy
    (
        0, -- Leading
        0, -- Waiting-Leading
        -- Leading data
        CAST('{"type": "leading", "data": "Leading-Data"}' AS JSON),
        NOW(),
        TRUE,
        -- Trailing data
        CAST(NULL AS JSON),
        CAST(NULL AS TIMESTAMPTZ),
        FALSE,
        -- Throttling data
        1, -- Resource #1
        2500, -- 2.5s
        FLOOR(EXTRACT(EPOCH FROM NOW()) * 1000 / 2500)
    )
) AS input_data(
    type,
    status,
    leading_payload,
    leading_produced_at,
    is_leading_pending,
    trailing_payload,
    trailing_produced_at,
    is_trailing_pending,
    resource,
    time_frame_size,
    time_frame_number
)
ON CONFLICT ON INDEX time_frame_throttling DO UPDATE SET
    trailing_payload = COALESCE(EXCLUDED.leading_payload, EXCLUDED.trailing_payload),
    trailing_produced_at = NOW(),
    is_trailing_pending = TRUE; -- Populating with data regardless