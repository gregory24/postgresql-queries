-- Cleaning Up
-- Plan: Nested-Loop Join for Deletion
EXPLAIN
DELETE FROM throttler
WHERE id IN (
    SELECT id FROM throttler
    WHERE status = 3 AND time_frame_end < NOW()
    LIMIT 5000
    FOR UPDATE SKIP LOCKED
);

-- Unlocking
-- Plan: Nested-Loop Join for Update
EXPLAIN
UPDATE throttler
SET locked_by = NULL, locked_until = NULL
WHERE id IN (
    SELECT id FROM throttler
    WHERE status = 1 AND locked_until < NOW()
    LIMIT 5000
    FOR UPDATE SKIP LOCKED
);
