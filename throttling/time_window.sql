DROP TABLE throttling_table_windows;

CREATE TABLE throttling_table_windows(
    record_id SERIAL PRIMARY KEY,
    data TEXT,
    at_time TIMESTAMPTZ,

    resource_id VARCHAR(255) NOT NULL,
    time_window_end TIMESTAMPTZ NOT NULL
);

-- Covers Searching and Sorting
--    Searching: Using Index-Condition - Using both columns
--    Sorting: Applying Index-Scan on the subset filtered by Index-Condition - Skipping the first column
-- Important: Make Time-Window-End Descending (Finding / Sorting the latest)
DROP INDEX throttler_windows;
CREATE UNIQUE INDEX throttler_windows ON throttling_table_windows
    USING BTREE (resource_id, time_window_end DESC);

-- Getting the current record that is throttling the incoming ones
-- Important: Sorting and limiting is NOT required - Automatically fulfilled by application conditions
EXPLAIN
SELECT resource_id FROM throttling_table_windows
WHERE resource_id = 'resource' AND time_window_end > NOW()
ORDER BY time_window_end DESC
LIMIT 1;

-- Adding a new record with a window (No current window is found)

INSERT INTO throttling_table_windows(data, at_time, resource_id, time_window_end)
VALUES (
    CAST(NOW() AS TEXT),
    NOW(),
    'resource',
    NOW() + (interval '1 second' * 10) -- Window of 10 seconds
);

-- Update  an existing record within the current window (A current window is found)
UPDATE throttling_table_windows
SET data = 'new_data', at_time = NOW()
WHERE record_id = 1;

-- Throttling (FIRST)
EXPLAIN
WITH check_results AS (
    SELECT resource_id FROM throttling_table_windows
    WHERE resource_id = 'resource' AND time_window_end > NOW()
    ORDER BY time_window_end DESC
    LIMIT 1
    FOR UPDATE SKIP LOCKED
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT data, at_time, resource_id, time_window_end FROM (VALUES
    (
        'inserted',
        NOW(),
        'resource',
        NOW() + (interval '1 second' * 5)
    )
) AS stats(data, at_time, resource_id, time_window_end)
WHERE resource_id NOT IN (SELECT resource_id FROM check_results)
RETURNING *;

-- Throttling (LAST)
EXPLAIN
WITH check_results AS (
    SELECT record_id FROM throttling_table_windows
    WHERE resource_id = 'resource' AND time_window_end > NOW()
    FOR UPDATE SKIP LOCKED
), update_results AS (
    UPDATE throttling_table_windows
    SET data = 'updated', at_time = NOW()
    FROM check_results
    WHERE throttling_table_windows.record_id = check_results.record_id
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT data, at_time, resource_id, time_window_end FROM (VALUES
    (
        'inserted',
        NOW(),
        'resource',
        NOW() + (interval '1 second' * 5)
    )
) AS stats(data, at_time, resource_id, time_window_end)
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;

-- Verifying
TRUNCATE throttling_table_windows;
SELECT * FROM throttling_table_windows;


-- Performance test (Insert into table with 5M)
-- Time-Window: 30s
-- Note: Only testing updates (Worst case scenario)
-----------------------------------------------

-- Population
TRUNCATE throttling_table_windows;
INSERT INTO throttling_table_windows(data, at_time, resource_id, time_window_end)
SELECT
       CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       CAST((i % 10) AS TEXT),
       NOW() - (interval '1 second' * i)
FROM generate_series(0, 5 * 1000 * 1000) AS s(i);

-- Test: Throttle with the First
--      With Returning: 250ms
--      Without Returning: 40ms
WITH check_results AS (
        SELECT record_id FROM throttling_table_windows
        WHERE resource_id = 'resource' AND time_window_end > NOW()
        FOR UPDATE SKIP LOCKED
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT data, at_time, resource_id, time_window_end FROM (VALUES
    (
        'inserted',
        NOW(),
        'resource',
        NOW() + (interval '1 second' * 5)
    )
) AS stats(data, at_time, resource_id, time_window_end)
WHERE resource_id NOT IN (SELECT resource_id FROM check_results)
RETURNING *;

-- Test: Throttle with the Last
--      With Returning: 300ms
--      Without Returning: 50ms
WITH update_results AS (
        UPDATE throttling_table_windows
        SET data = 'new_data', at_time = NOW()
        FROM (
            SELECT record_id FROM throttling_table_windows
            WHERE resource_id = 'resource' AND time_window_end > NOW()
            ORDER BY time_window_end DESC
            LIMIT 1
            FOR UPDATE SKIP LOCKED
        ) AS targets
        WHERE throttling_table_windows.record_id = targets.record_id
        RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT data, at_time, resource_id, time_window_end FROM (VALUES
    (
        'old_data',
        NOW(),
        'resource',
        NOW() + (interval '1 second' * 5)
    )
) AS stats(data, at_time, resource_id, time_window_end)
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;