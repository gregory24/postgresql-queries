DROP TABLE throttling_table_windows;

CREATE TABLE throttling_table_windows(
    record_id SERIAL PRIMARY KEY,
    data TEXT,
    at_time TIMESTAMPTZ,

    resource_id VARCHAR(255) NOT NULL,
    time_window_end TIMESTAMPTZ NOT NULL
);

-- Covers Searching and Sorting
--    Searching: Using Index-Condition - Using both columns
--    Sorting: Applying Index-Scan on the subset filtered by Index-Condition - Skipping the first column
-- Important: Make Time-Window-End Descending (Finding / Sorting the latest)
-- TODO: Find the best indexing strategy (Single Compound vs Multiple Individual)
DROP INDEX throttler_windows;
CREATE UNIQUE INDEX throttler_windows ON throttling_table_windows
    USING BTREE (resource_id, time_window_end DESC);
DROP INDEX throttler_windows_resource;
CREATE INDEX throttler_windows_resource ON throttling_table_windows
    USING BTREE (resource_id);
DROP INDEX throttler_windows_time;
CREATE INDEX throttler_windows_time ON throttling_table_windows
    USING BTREE (time_window_end DESC);

-- Getting the current record that is throttling the incoming ones

EXPLAIN
SELECT record_id FROM throttling_table_windows
WHERE resource_id = 'resource' AND time_window_end > NOW()
ORDER BY time_window_end DESC
LIMIT 1;

-- Adding a new record with a window (No current window is found)

INSERT INTO throttling_table_windows(data, at_time, resource_id, time_window_end)
VALUES (
    CAST(NOW() AS TEXT),
    NOW(),
    'resource',
    NOW() + (interval '1 second' * 10) -- Window of 10 seconds
);

-- Update  an existing record within the current window (A current window is found)
UPDATE throttling_table_windows
SET data = 'new_data', at_time = NOW()
WHERE record_id = 1;

-- Throttling (FIRST)
EXPLAIN
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), check_results AS (
    SELECT resource_id FROM throttling_table_windows
    WHERE time_window_end > NOW() AND resource_id IN ('resource1', 'resource2', 'resource3')
    FOR UPDATE SKIP LOCKED
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM check_results)
RETURNING *;

-- Throttling (LAST)
EXPLAIN
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), update_results AS (
    UPDATE throttling_table_windows
    SET data = new_data.data, at_time = new_data.at_time
    FROM new_data
    WHERE throttling_table_windows.resource_id = new_data.resource_id
        AND throttling_table_windows.time_window_end > NOW()
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;

-- Debouncing
EXPLAIN
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), update_results AS (
    UPDATE throttling_table_windows
    SET
        data = new_data.data,
        at_time = new_data.at_time,
        time_window_end = new_data.time_window_end -- Updating the window for debouncing purposes
    FROM new_data
    WHERE throttling_table_windows.resource_id = new_data.resource_id
        AND throttling_table_windows.time_window_end > NOW()
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;

-- Verifying
TRUNCATE throttling_table_windows;
SELECT * FROM throttling_table_windows;


-- Performance test (Insert into table with 5M)
-- Time-Window: 5s
-- TODO: Experimenting with using / avoiding CTEs (Especially for values)
--   -> In code with lots of values
-----------------------------------------------

-- Population
TRUNCATE throttling_table_windows;
INSERT INTO throttling_table_windows(data, at_time, resource_id, time_window_end)
SELECT
       CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       CAST((i % 10) AS TEXT),
       NOW() - (interval '1 second' * i)
FROM generate_series(0, 5 * 1000 * 1000) AS s(i);

-- Test: Throttle with the First
--      With Returning: 250ms
--      Without Returning: 30ms
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), check_results AS (
    SELECT resource_id FROM throttling_table_windows
    WHERE time_window_end > NOW() AND resource_id IN ('resource1', 'resource2', 'resource3')
    FOR UPDATE SKIP LOCKED
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM check_results)
RETURNING *;

-- Test: Throttle with the Last
--      With Returning: 250ms
--      Without Returning: 20ms
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), update_results AS (
    UPDATE throttling_table_windows
    SET data = new_data.data, at_time = new_data.at_time
    FROM new_data
    WHERE throttling_table_windows.resource_id = new_data.resource_id
              AND throttling_table_windows.time_window_end > NOW()
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;

-- Test: Debouncing
--      With Returning: 250ms
--      Without Returning: 20ms
WITH new_data AS (
    SELECT data, at_time, resource_id, time_window_end FROM (VALUES
        ('1', NOW(), 'resource1', NOW() + (interval '1 second' * 3)),
        ('2', NOW(), 'resource2', NOW() + (interval '1 second' * 5)),
        ('3', NOW(), 'resource3', NOW() + (interval '1 second' * 7))
    ) AS stats(data, at_time, resource_id, time_window_end)
), update_results AS (
    UPDATE throttling_table_windows
    SET
        data = new_data.data,
        at_time = new_data.at_time,
        time_window_end = new_data.time_window_end -- Updating the window for debouncing purposes
    FROM new_data
    WHERE throttling_table_windows.resource_id = new_data.resource_id
              AND throttling_table_windows.time_window_end > NOW()
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM update_results)
RETURNING *;


--------------------------------------
-- Benchmarking
--------------------------------------

-- Options

SET enable_seqscan = FALSE;
SET enable_seqscan = TRUE;

SET enable_hashjoin = FALSE;
SET enable_hashjoin = TRUE;


-- Optimizations
--------------------------------------

-- Compound Index (WORKS GREAT)
DROP INDEX IF EXISTS throttler_windows;

CREATE INDEX throttler_windows ON throttling_table_windows
    USING BTREE (resource_id, time_window_end DESC);

-- Separate Indexes (WORKS TERRIBLE)
DROP INDEX IF EXISTS throttler_windows_resource;
DROP INDEX IF EXISTS throttler_windows_time;

CREATE INDEX throttler_windows_resource ON throttling_table_windows USING BTREE (resource_id);
CREATE INDEX throttler_windows_time ON throttling_table_windows USING BTREE (time_window_end DESC);


-- Input
--------------------------------------
CREATE TABLE windows_input_data(
    data TEXT,
    at_time TIMESTAMPTZ,
    resource_id VARCHAR(255) NOT NULL,
    time_window_size INTEGER NOT NULL
);

INSERT INTO windows_input_data (data, at_time, resource_id, time_window_size)
SELECT
       'data' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST(i AS TEXT),
       i % 10
FROM generate_series(1, 100) AS series(i);


-- Data and Population
--------------------------------------

TRUNCATE throttling_table_windows;

-- Old Time-Frames
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT
       'initial' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST((i % 500) AS TEXT),
       NOW() - (interval '1 second' * (i % 300))
FROM generate_series(1, 20 * 1000 * 1000) AS series(i);

-- Simulating Best-Case (ALL INSERTS)
UPDATE throttling_table_windows
SET time_window_end = NOW()
WHERE time_window_end > NOW();

-- Simulating Average-Case
-- Open Time-Frames (Only half of them is open) -> 50 Updates and 50 Inserts
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT
       'initial' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST(i AS TEXT),
       NOW() + (interval '1 hour' * (i % 2))
FROM generate_series(1, 100) AS series(i);

-- Simulating Worst-Case (ALL UPDATES)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT
       'initial' || CAST(i AS TEXT),
       NOW() - (interval '1 second' * i),
       'resource' || CAST(i AS TEXT),
       NOW() + interval '1 hour'
FROM generate_series(1, 100) AS series(i);


-- Querying
--------------------------------------

-- First Strategy

EXPLAIN (VERBOSE)
WITH new_data AS (
    SELECT data, at_time, resource_id, (
        NOW() + (interval '1 second' * time_window_size)
    ) AS time_window_end FROM windows_input_data
), check_results AS (
    SELECT resource_id FROM throttling_table_windows
    WHERE time_window_end > NOW() AND resource_id IN (SELECT resource_id FROM new_data)
    FOR UPDATE SKIP LOCKED
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM check_results);

-- Last Strategy

EXPLAIN (VERBOSE)
WITH new_data AS (
    SELECT data, at_time, resource_id, (
        NOW() + (interval '1 second' * time_window_size)
    ) AS time_window_end FROM windows_input_data
), update_results AS (
    UPDATE throttling_table_windows
    SET data = new_data.data, at_time = new_data.at_time
    FROM new_data
    WHERE throttling_table_windows.resource_id = new_data.resource_id
              AND throttling_table_windows.time_window_end > NOW()
    RETURNING throttling_table_windows.resource_id
)
INSERT INTO throttling_table_windows (data, at_time, resource_id, time_window_end)
SELECT * FROM new_data
WHERE resource_id NOT IN (SELECT resource_id FROM update_results);
