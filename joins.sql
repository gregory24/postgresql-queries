-- Data Models

CREATE TABLE join_tester(
    key1 INTEGER NOT NULL, -- NOT UNIQUE

    key2 INTEGER NOT NULL -- UNIQUE
);

CREATE INDEX r_index ON join_tester USING BTREE (key1);

CREATE UNIQUE INDEX u_index ON join_tester USING BTREE (key2);

CREATE TABLE join_tester_assist(key INTEGER PRIMARY KEY); -- Auto-creates a UNIQUE index on the key

CREATE TABLE join_tester_assist2(key INTEGER); -- Index on "key" is not UNIQUE

CREATE INDEX assist_index ON join_tester_assist2 USING BTREE (key);

-- Population

TRUNCATE join_tester;
TRUNCATE join_tester_assist;
TRUNCATE join_tester_assist2;

INSERT INTO join_tester(key1, key2)
SELECT i / 2, i FROM generate_series(0, 10 * 1000 * 1000) AS series(i);

INSERT INTO join_tester_assist(key)
SELECT i FROM generate_series(0, 1000 * 1000, 10) AS series(i);

INSERT INTO join_tester_assist2(key)
SELECT i FROM generate_series(0, 1000 * 1000, 10) AS series(i);

SET enable_seqscan = FALSE;

-- Both use inner join (Thanks to assist table)

-- Merge (Inner) Join -> 69764 IO / 250ms
-- Inner UNIQUE -> Inner Join
EXPLAIN (VERBOSE)
SELECT * FROM join_tester
WHERE key1 IN (SELECT key FROM join_tester_assist);

-- Merge (Inner) Join -> 36170 IO / 200ms
-- Inner UNIQUE -> Inner Join
EXPLAIN (VERBOSE)
SELECT * FROM join_tester
WHERE key2 IN (SELECT key FROM join_tester_assist);

-- Merge SEMI Join -> 69764 IO / 200ms
-- Inner NOT UNIQUE -> Left Semi Join
EXPLAIN (VERBOSE)
SELECT * FROM join_tester
WHERE key1 IN (SELECT key FROM join_tester_assist2);

-- Merge SEMI Join -> 36170 IO / 200ms
-- Inner NOT UNIQUE -> Left Semi Join
EXPLAIN (VERBOSE)
SELECT * FROM join_tester
WHERE key2 IN (SELECT key FROM join_tester_assist2);
