SET enable_hashjoin = FALSE;

-- Performance: 19.5s, 18s, 18s -> 18.5s

BEGIN;

CREATE TEMPORARY TABLE full_data ON COMMIT DROP AS (
    SELECT
        -- Key-Column Logic
        (CASE mode WHEN 2 THEN state_table_key ELSE input_table_key END) AS key,
        -- Data-Columns logic
        (CASE
            WHEN (mode = 0 AND is_changed) THEN input_table_data1
            WHEN (mode = 0 AND NOT is_changed) THEN state_table_data1
            WHEN mode = 1 THEN input_table_data1
            ELSE state_table_data1
        END) AS data1,
        (CASE
            WHEN (mode = 0 AND is_changed) THEN input_table_data2
            WHEN (mode = 0 AND NOT is_changed) THEN state_table_data2
            WHEN mode = 1 THEN input_table_data2
            ELSE state_table_data2
        END) AS data2,
        -- Latest-Write-Timestamp logic
        (CASE
            WHEN (mode = 0 AND is_changed) THEN input_table_latest_write
            WHEN (mode = 0 AND NOT is_changed) THEN state_table_latest_write
            WHEN mode = 1 THEN input_table_latest_write
            ELSE state_table_latest_write
        END) AS latest_write,
        LEAST(input_table_first_write, state_table_first_write) AS first_write,
        mode,
        is_changed
    FROM (
        SELECT
               input_table.key AS input_table_key,
               input_table.data1 AS input_table_data1,
               input_table.data2 AS input_table_data2,
               input_table.latest_write AS input_table_latest_write,
               input_table.first_write AS input_table_first_write,

               state_table.key AS state_table_key,
               state_table.data1 AS state_table_data1,
               state_table.data2 AS state_table_data2,
               state_table.latest_write AS state_table_latest_write,
               state_table.first_write AS state_table_first_write,

               (CASE
                   WHEN (input_table.key IS NOT NULL AND state_table.key IS NOT NULL) THEN 0 -- Update
                   WHEN input_table.key IS NOT NULL THEN 1 -- Insert
                   ELSE 2 -- Delete
               END) AS mode,
               (
                   input_table.key IS NOT NULL AND state_table.key IS NOT NULL
                    AND (input_table.latest_write > state_table.latest_write)
                    AND (
                        input_table.data1 IS DISTINCT FROM state_table.data1 OR
                        input_table.data2 IS DISTINCT FROM state_table.data2
                    )
               ) AS is_changed
        FROM input_table
        FULL OUTER JOIN state_table ON input_table.key = state_table.key
    ) AS change_data
);

CREATE TEMPORARY TABLE updated ON COMMIT DROP AS (
    SELECT key, data1, data2, first_write, latest_write FROM full_data
    WHERE mode = 0 AND is_changed
);

CREATE TEMPORARY TABLE created ON COMMIT DROP AS (
    SELECT key, data1, data2, first_write, latest_write FROM full_data
    WHERE mode = 1
);

CREATE TEMPORARY TABLE deleted ON COMMIT DROP AS (
    SELECT key, data1, data2, first_write, latest_write FROM full_data
    WHERE mode = 2
);

INSERT INTO state_table
SELECT key, data1, data2, first_write, latest_write FROM full_data
WHERE mode IN (0, 1);

COMMIT;