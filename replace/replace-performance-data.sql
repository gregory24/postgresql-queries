TRUNCATE state_table;

INSERT INTO state_table(key, data1, data2, first_write, latest_write)
SELECT i, i, i, TO_TIMESTAMP(i), TO_TIMESTAMP(i)
FROM generate_series(1, 3000 * 1000) AS series(i);

TRUNCATE input_table;

INSERT INTO input_table(key, data1, data2, first_write, latest_write)
SELECT i, i + 1, i + 1, TO_TIMESTAMP(i + 1), TO_TIMESTAMP(i + 1)
FROM generate_series(1, 2 * 3000 * 1000, 2) AS series(i);
