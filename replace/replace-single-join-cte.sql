SET enable_hashjoin = FALSE;

-- Performance: 17s, 18s, 18s -> 17.5s

BEGIN;

CREATE TEMPORARY TABLE updated (LIKE input_table EXCLUDING CONSTRAINTS) ON COMMIT DROP;
CREATE TEMPORARY TABLE created (LIKE input_table EXCLUDING CONSTRAINTS) ON COMMIT DROP;
CREATE TEMPORARY TABLE deleted (LIKE input_table EXCLUDING CONSTRAINTS) ON COMMIT DROP;

CREATE TEMPORARY TABLE new_state ON COMMIT DROP AS (
    WITH full_data AS (
        SELECT
            -- Key-Column Logic
            (CASE mode WHEN 2 THEN state_table_key ELSE input_table_key END) AS key,
            -- Data-Columns logic
            (CASE
                WHEN (mode = 0 AND is_changed) THEN input_table_data1
                WHEN (mode = 0 AND NOT is_changed) THEN state_table_data1
                WHEN mode = 1 THEN input_table_data1
                ELSE state_table_data1
            END) AS data1,
            (CASE
                WHEN (mode = 0 AND is_changed) THEN input_table_data2
                WHEN (mode = 0 AND NOT is_changed) THEN state_table_data2
                WHEN mode = 1 THEN input_table_data2
                ELSE state_table_data2
            END) AS data2,
            -- Latest-Write-Timestamp logic
            (CASE
                WHEN (mode = 0 AND is_changed) THEN input_table_latest_write
                WHEN (mode = 0 AND NOT is_changed) THEN state_table_latest_write
                WHEN mode = 1 THEN input_table_latest_write
                ELSE state_table_latest_write
            END) AS latest_write,
            -- First-Write-Timestamp logic
            LEAST(input_table_first_write, state_table_first_write) AS first_write,
            -- Special Columns
            mode, -- Determining type of change (If any)
            is_changed -- Determining whether the new data is changing the old data (Update-Only)
        FROM (
            SELECT
                   input_table.key AS input_table_key,
                   input_table.data1 AS input_table_data1,
                   input_table.data2 AS input_table_data2,
                   input_table.latest_write AS input_table_latest_write,
                   input_table.first_write AS input_table_first_write,

                   state_table.key AS state_table_key,
                   state_table.data1 AS state_table_data1,
                   state_table.data2 AS state_table_data2,
                   state_table.latest_write AS state_table_latest_write,
                   state_table.first_write AS state_table_first_write,

                   (CASE
                       WHEN (input_table.key IS NOT NULL AND state_table.key IS NOT NULL) THEN 0 -- Update
                       WHEN input_table.key IS NOT NULL THEN 1 -- Insert
                       ELSE 2 -- Delete
                   END) AS mode,
                   (
                       input_table.key IS NOT NULL AND state_table.key IS NOT NULL
                        AND (input_table.latest_write > state_table.latest_write)
                        AND (
                            input_table.data1 IS DISTINCT FROM state_table.data1 OR
                            input_table.data2 IS DISTINCT FROM state_table.data2 OR
                            -- TODO: MAKE SURE
                            input_table.latest_write IS DISTINCT FROM state_table.latest_write
                        )
                   ) AS is_changed
            FROM input_table
            FULL OUTER JOIN state_table ON input_table.key = state_table.key
        ) AS change_data
    ), __updated_modification AS (
        INSERT INTO updated
        SELECT key, data1, data2, first_write, latest_write FROM full_data
        WHERE mode = 0 AND is_changed
    ), __created_modification AS (
        INSERT INTO updated
        SELECT key, data1, data2, first_write, latest_write FROM full_data
        WHERE mode = 1
    ), __deleted_modification AS (
        INSERT INTO updated
        SELECT key, data1, data2, first_write, latest_write FROM full_data
        WHERE mode = 2
    )
    SELECT key, data1, data2, first_write, latest_write FROM full_data
    WHERE mode IN (0, 1)
);

TRUNCATE state_table;

INSERT INTO state_table
SELECT * FROM new_state;

COMMIT;
