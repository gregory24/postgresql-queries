-- Performance: 17.5s, 16.5s, 15.5s -> 16.5s

-- IMPORTANT: NOT COMPLETE - CANNOT WORK WITH TIMESTAMP-LOGIC AND DATA-CHANGES (Efficiently)

SET enable_hashjoin = FALSE;

BEGIN;

CREATE INDEX input_idx ON input_table(key);

CREATE TEMPORARY TABLE updated ON COMMIT DROP AS (
    SELECT input_table.* FROM input_table
    INNER JOIN state_table ON input_table.key = state_table.key
);

CREATE TEMPORARY TABLE created ON COMMIT DROP AS (
    SELECT key, data1, data2, first_write, latest_write FROM (
        SELECT
            input_table.*,
            (CASE
            WHEN state_table.key IS NULL THEN TRUE
            ELSE FALSE
            END) AS __is_new
        FROM input_table
        LEFT JOIN state_table ON input_table.key = state_table.key
    ) AS data_with_match_flags
    WHERE __is_new IS TRUE
);

CREATE TEMPORARY TABLE deleted ON COMMIT DROP AS (
    SELECT key, data1, data2, first_write, latest_write FROM (
        SELECT
            state_table.*,
            (CASE
            WHEN input_table.key IS NULL THEN TRUE
            ELSE FALSE
            END) AS __is_new
        FROM state_table
        LEFT JOIN input_table ON input_table.key = state_table.key
    ) AS data_with_match_flags
    WHERE __is_new IS TRUE
);

DROP INDEX input_idx;

TRUNCATE state_table;

INSERT INTO state_table
SELECT * FROM (
    SELECT * FROM created
    UNION ALL
    SELECT * FROM updated
) AS full_data;

COMMIT;