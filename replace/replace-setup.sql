-- Tables

DROP TABLE state_table;

CREATE TABLE state_table (
    key INTEGER NOT NULL,
    data1 TEXT,
    data2 TEXT,
    first_write TIMESTAMP NOT NULL,
    latest_write TIMESTAMP NOT NULL
);

DROP TABLE input_table;

CREATE TABLE input_table (
    LIKE state_table
);

-- Indexes

CREATE INDEX state_idx ON state_table(key);