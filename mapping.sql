DROP TABLE transformed_data;

CREATE TABLE raw_data AS (
  SELECT i AS c1, i * 2 AS c2, i::TEXT AS c3 FROM generate_series(1, 1000 * 100) AS series(i)
);


CREATE TEMPORARY TABLE transformed_data AS (
  SELECT c1 AS m1, c2 AS m2, c3 AS m3 FROM raw_data
);
