CREATE TABLE multikey_pk_tester(
    id INTEGER,
    val INTEGER,

    -- Compound Primary Key
    CONSTRAINT multikey_pk_tester_pk PRIMARY KEY (id, val)
);

CREATE TABLE multikey_pk_tester_data(
    id INTEGER,
    val INTEGER
);

CREATE INDEX multikey_pk_index ON multikey_pk_tester_data (id, val);

-- IMPORTANT: Can provide column-names / labels for functions / values on the fly
INSERT INTO multikey_pk_tester(id, val)
SELECT series, series * 2 FROM generate_series(0, 1000 * 1000) AS g(series);

INSERT INTO multikey_pk_tester_data(id, val)
VALUES (1, 2), (5, 10), (100, 200), (1000, 2000);

EXPLAIN ANALYZE
SELECT * FROM multikey_pk_tester
WHERE EXISTS (
    -- IMPORTANT: Can provide column-names / labels for functions / values on the fly
    SELECT 1 FROM (VALUES (1, 2), (500000, 1000000), (900000, 1800000)) AS vals(id, key)
    WHERE multikey_pk_tester.id = vals.id AND multikey_pk_tester.val = vals.key
);



EXPLAIN ANALYZE
SELECT id FROM multikey_pk_tester
WHERE EXISTS (
    SELECT 1 FROM multikey_pk_tester_data
    WHERE multikey_pk_tester.id = multikey_pk_tester_data.id
        AND multikey_pk_tester.val = multikey_pk_tester_data.val
);
