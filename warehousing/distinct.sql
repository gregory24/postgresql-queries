DROP TABLE IF EXISTS dedupe;
DROP TABLE IF EXISTS dedupe_heavy;

CREATE TABLE dedupe(a, b, c) AS
    SELECT * FROM (VALUES
        (1, 3, 5),
        (1, 4, 6),
        (2, 4, 7),
        (2, 4, 8)
    ) AS data;

CREATE TABLE dedupe_heavy (a, b, c) AS
    SELECT
           series AS a,
           CAST(random() AS INTEGER) % 5 AS b,
           CAST(random() AS INTEGER) % 10 AS c
    FROM generate_series(0, 1000 * 1000) AS g(series);

CREATE INDEX dedupe_heavy_index ON dedupe_heavy USING BTREE (a ASC);
CREATE INDEX dedupe_heavy_index_2 ON dedupe_heavy USING BTREE (b ASC, c ASC);


-- Distinct combinations
SELECT DISTINCT a, b, c FROM dedupe;

-- Distinct based on a column
--    Distinction: Using column a with unique values - FIRST occurrence (Unpredictable)
--    Output: Distinction column is NOT selected into the output itself
SELECT DISTINCT ON (a) b, c FROM dedupe;

-- Distinct based on a combination of columns -> Including all columns separately
SELECT DISTINCT ON (a, b) a, b, c FROM dedupe;


-- Distinct with ordering -> Allows selecting which Record in the group is used exactly
-- Important: Distinct-Column(s) MUST be included into Ordering
--     Do NOT: Affect which Records / Rows from their groups get selected
--     Do: Determine order of grouped and distinct values
-- Note: Does NOT matter which ordering is used as long as Distinct-Columns are
--     1) Included into the ordering expression (ALL)
--     2) FIRST in the expression (Come before Record-Selection Columns)
SELECT DISTINCT ON (a) a, b, c FROM dedupe
ORDER BY a, c DESC;
-- Would NOT work: ORDER BY b, a

-- Same with multiple columns
SELECT DISTINCT ON (a, b) a, b, c FROM dedupe
ORDER BY a, b, c DESC;

-- Only changes ordering of the output - Does NOT affect which Records get chosen from their Groups
SELECT DISTINCT ON (a, b) a, b, c FROM dedupe
ORDER BY a DESC, b DESC;


-- Aggregations
----------------------------------------------------------

SELECT COUNT(a) FROM dedupe;
SELECT COUNT(DISTINCT a) FROM dedupe;

SELECT ARRAY_AGG(a) FROM dedupe;
SELECT SUM(DISTINCT a ORDER BY a DESC) FROM dedupe;

SELECT ARRAY_AGG(a) FROM dedupe;
SELECT ARRAY_AGG(DISTINCT a ORDER BY a DESC) FROM dedupe;


-- Analysis (With / Without Ordering)
-- Internals:
--    1) Sort by (Quick / External Merge / etc.) OR Index-Scan (If one is present)
--        a) No Ordering: Distinct-Key (ASC for all)
--        b) Specified Ordering: Ordering Specification (Includes Distinct-Key)
--    2) Unique - Taking First (According to the Ordering from Sorting / Index-Scan)
----------------------------------------------------------

-- Light: Using In-Memory Quick-Sort

EXPLAIN ANALYZE
SELECT DISTINCT ON (a) a, b, c FROM dedupe;

EXPLAIN ANALYZE
SELECT DISTINCT ON (a) a, b, c FROM dedupe
ORDER BY a, c DESC;

-- Heavy: Using Indexes when matching Sorting (Used by Distinct regardless of whether explicit Ordering is provided)

EXPLAIN ANALYZE
SELECT DISTINCT ON (a) a, b, c FROM dedupe_heavy;

EXPLAIN ANALYZE
SELECT DISTINCT ON (b, c) a, b, c FROM dedupe_heavy
ORDER BY b, c, a DESC;

-- Heavy: No Index -> Using External Merge-Sort

EXPLAIN ANALYZE
SELECT DISTINCT ON (a) a, b, c FROM dedupe_heavy
ORDER BY a, b;


-- Alternative (Requires extension / creating procedures)
-- Note: Define for a session / transaction?
----------------------------------------------------------

SELECT a, LAST(b) AS b, LAST(c) AS c FROM dedupe GROUP BY a ORDER BY a, b, c;

SELECT a, b, FIRST(c) AS c FROM dedupe GROUP BY a, b;
