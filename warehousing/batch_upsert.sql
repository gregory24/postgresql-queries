-- Preparation
--------------------------------------------------

CREATE TABLE dest_table(
    id INTEGER PRIMARY KEY,
    val INTEGER,
    name VARCHAR(255),
    info TEXT
);

CREATE TABLE source_table(
    id INTEGER,
    val INTEGER,
    name VARCHAR(255),
    info TEXT
);

-- Utilities
--------------------------------------------------

SELECT COUNT(1) FROM source_table;
SELECT COUNT(1) FROM dest_table;

-- Population
--------------------------------------------------

-- Source Table

TRUNCATE source_table;

-- All values between 0 and 1M => 1.5M total
INSERT INTO source_table(id, val, name, info)
SELECT index, 0, index || '-Name', index || '-Info'
FROM generate_series(0, 1000 * 1000, 1) AS series(index);

-- Extra: 500K duplicates
INSERT INTO source_table(id, val, name, info)
SELECT index, 0, index || '-Name', index || '-Info'
FROM generate_series(0, 1000 * 1000, 2) AS series(index);

-- Destination Table

TRUNCATE dest_table;

-- Only Even between 0 and 2M => 1M total
INSERT INTO dest_table(id, val, name, info)
SELECT index, index + 5, index || '-Name', index || '-Info'
FROM generate_series(0, 2 * 1000 * 1000, 2) AS series(index);

-- Method 1: Single Copy-Table with Ignoring
--------------------------------------------------

BEGIN;

CREATE TEMPORARY TABLE copy_table ON COMMIT DROP AS (
    SELECT DISTINCT ON (id) * FROM source_table
    ORDER BY id
);

CREATE INDEX pk_key ON copy_table USING BTREE (id);

UPDATE dest_table
SET val = copy_table.val, name = copy_table.name, info = copy_table.info
FROM copy_table
WHERE dest_table.id = copy_table.id;

INSERT INTO dest_table
SELECT * FROM copy_table
ON CONFLICT ON CONSTRAINT dest_table_pkey DO NOTHING;

COMMIT;

-- Method 2: Full CTE

BEGIN;

WITH dedupe_data AS (
    SELECT DISTINCT ON (id) * FROM source_table
    ORDER BY id
), updates AS (
    UPDATE dest_table
    SET val = dedupe_data.val, name = dedupe_data.name, info = dedupe_data.info
    FROM dedupe_data
    WHERE dest_table.id = dedupe_data.id
    RETURNING dedupe_data.id
), inserts AS (
    INSERT INTO dest_table
    SELECT * FROM dedupe_data
    ON CONFLICT ON CONSTRAINT dest_table_pkey DO NOTHING
)
SELECT 1;

COMMIT;

-- Method 3: Separate Tables for Insert and Update

BEGIN;

CREATE TEMPORARY TABLE update_table(
    id INTEGER, -- TODO: PK???
    val INTEGER,
    name VARCHAR(255),
    info TEXT
) ON COMMIT DROP;

CREATE TEMPORARY TABLE insert_table(
    id INTEGER,
    val INTEGER,
    name VARCHAR(255),
    info TEXT
) ON COMMIT DROP;

WITH dedupe_data AS (
    SELECT DISTINCT ON (id) * FROM source_table
    ORDER BY id
), matched_records AS (
    SELECT id FROM dedupe_data
    WHERE id IN (SELECT id FROM dest_table)
), updates AS (
    INSERT INTO update_table
    SELECT * FROM source_table
    WHERE id IN (SELECT id FROM matched_records)
), inserts AS (
    INSERT INTO insert_table
    SELECT * FROM source_table
--     WHERE id NOT IN (SELECT id FROM matched_records)
)
SELECT 1;

UPDATE dest_table
SET val = update_table.val, name = update_table.name, info = update_table.info
FROM update_table
WHERE dest_table.id = update_table.id;

INSERT INTO dest_table
SELECT * FROM insert_table
ON CONFLICT ON CONSTRAINT dest_table_pkey DO NOTHING;

COMMIT;
