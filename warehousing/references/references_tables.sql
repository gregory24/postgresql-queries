DROP TABLE IF EXISTS data_artefact_logs;

CREATE TABLE IF NOT EXISTS data_artefact_logs (
    data_id VARCHAR,
    prototype_id UUID,
    storage_id UUID,

    log_type INTEGER, -- 0 (Produced) / 1 (Consumed)

    source UUID,
    destination UUID,

    logged_at TIMESTAMPTZ DEFAULT NOW()

     -- IMPORTANT: NO PK - Only Index (Allowing multiple logs for a single DA)
);

CREATE INDEX data_artefact_logs_id_index ON data_artefact_logs(data_id, prototype_id, storage_id);

CREATE TABLE IF NOT EXISTS data_artefacts (
    data_id VARCHAR,
    prototype_id UUID,
    storage_id UUID,

    metadata JSON,

    created_at TIMESTAMPTZ DEFAULT NOW(),

    CONSTRAINT data_artefacts_pk PRIMARY KEY (data_id, prototype_id, storage_id)
);

CREATE INDEX data_artefacts_time_index ON data_artefacts(created_at);
