INSERT INTO data_artefacts (data_id, prototype_id, storage_id, metadata)
VALUES ('1', '8171223d-ceec-4b5b-96e0-953e7a04a69d', '8171223d-ceec-4b5b-96e0-953e7a04a69d', '{}')
ON CONFLICT DO NOTHING;

INSERT INTO data_artefact_logs (data_id, prototype_id, storage_id, log_type, source, destination)
VALUES
       (
        '1',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        0,
        '89bc3d1a-2ba6-4fff-8fb9-ef41f7c30b5a',
        '0942d96d-6568-4b6e-84c1-03e560f1676e'
        ),
       (
        '1',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        0,
        '89bc3d1a-2ba6-4fff-8fb9-ef41f7c30b5a',
        '025aeb4c-b3fd-4d85-89a0-12f8f451a15e'
        ),
       (
        '1',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        1,
        NULL,
        '0942d96d-6568-4b6e-84c1-03e560f1676e'
        );

INSERT INTO data_artefact_logs (data_id, prototype_id, storage_id, log_type, source, destination)
VALUES (
        '1',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        '8171223d-ceec-4b5b-96e0-953e7a04a69d',
        1,
        '89bc3d1a-2ba6-4fff-8fb9-ef41f7c30b5a',
        '025aeb4c-b3fd-4d85-89a0-12f8f451a15e'
        );

