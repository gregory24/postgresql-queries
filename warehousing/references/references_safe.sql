-- Finding DAs that have been fully consumed
SELECT
    data_id,
    prototype_id,
    storage_id
FROM data_artefact_logs
GROUP BY data_id, prototype_id, storage_id
HAVING COUNT(DISTINCT (CASE log_type WHEN 0 THEN destination ELSE NUll END)) =
    COUNT(DISTINCT (CASE log_type WHEN 1 THEN destination ELSE NUll END))
LIMIT 1;

EXPLAIN
SELECT * FROM data_artefacts
WHERE EXISTS (
    SELECT 1 FROM (
        SELECT
            data_id,
            prototype_id,
            storage_id
        FROM data_artefact_logs
        GROUP BY data_id, prototype_id, storage_id
        HAVING COUNT(DISTINCT (CASE log_type WHEN 0 THEN destination ELSE NUll END)) =
            COUNT(DISTINCT (CASE log_type WHEN 1 THEN destination ELSE NUll END))
    ) AS safe_data_artefacts
    WHERE data_artefacts.data_id = safe_data_artefacts.data_id
    AND data_artefacts.prototype_id = safe_data_artefacts.prototype_id
    AND data_artefacts.storage_id = safe_data_artefacts.storage_id
);
