CREATE TABLE conflict_tests(
    id INTEGER,
    text TEXT,

    CONSTRAINT conflict_pk PRIMARY KEY (id)
);

TRUNCATE conflict_tests;
INSERT INTO conflict_tests(id, text)
SELECT series, CAST(series AS TEXT) FROM generate_series(0, 1000 * 1000) AS g(series);

SELECT * FROM conflict_tests;

INSERT INTO conflict_tests(id, text) VALUES (3, 'd')
ON CONFLICT ON CONSTRAINT conflict_pk DO NOTHING
RETURNING *; -- Returning rows that were ACTUALLY inserted

INSERT INTO conflict_tests(id, text) VALUES (3, 'd'), (6, 'a')
ON CONFLICT ON CONSTRAINT conflict_pk DO UPDATE
SET text = EXCLUDED.text
WHERE conflict_tests.id < 5 AND EXCLUDED.text != 'e'
RETURNING *; -- Returning rows that were EITHER inserted or updated

INSERT INTO conflict_tests(id, text) VALUES (3, 'e')
ON CONFLICT ON CONSTRAINT conflict_pk DO UPDATE
SET text = EXCLUDED.text
WHERE conflict_tests.id < 5 AND EXCLUDED.text != 'e';

-- LESS PERFORMANT (~8s)
-- Important: Updates do NOT get batched properly
INSERT INTO conflict_tests(id, text)
SELECT series * 2, 'X' FROM generate_series(0, 1000 * 1000) AS g(series)
ON CONFLICT ON CONSTRAINT conflict_pk DO UPDATE
SET text = EXCLUDED.text;

-- MORE PERFORMANT (~6s)
-- Important: Update before inserting - Less records for update-operation to affect
UPDATE conflict_tests
SET text = batch.text
FROM (SELECT series * 2 AS id, 'X' AS text FROM generate_series(0, 1000 * 1000) AS g(series)) AS batch
WHERE conflict_tests.id = batch.id;

INSERT INTO conflict_tests(id, text)
SELECT series * 2, 'X' FROM generate_series(0, 1000 * 1000) AS g(series)
ON CONFLICT ON CONSTRAINT conflict_pk DO NOTHING;
