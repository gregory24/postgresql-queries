CREATE TABLE cte_tester(
  id INTEGER PRIMARY KEY,
  text TEXT NOT NULL
);

TRUNCATE cte_tester;
INSERT INTO cte_tester(id, text) VALUES (1, 'a'), (2, 'b'), (3, 'c');

-- ORDER IS COMPLETELY UNPREDICTABLE + ALL OF THEM USE THE SAME SNAPSHOT OF THE MODIFICATION-TABLE
--    -> Intermediate changes from one CTE are NOT visible in other CTEs
WITH inserts AS (
    INSERT INTO cte_tester(id, text)
    VALUES (1, 'x'), (4, 'd'), (5, 'e')
    ON CONFLICT ON CONSTRAINT cte_tester_pkey DO NOTHING
    RETURNING id
),
updates AS (
    UPDATE cte_tester
    SET text = batch.text
    FROM (VALUES (1, 'x'), (4, 'd'), (5, 'e')) AS batch(id, text)
    WHERE cte_tester.id = batch.id
    RETURNING cte_tester.id
),
deletes AS (
    DELETE FROM cte_tester
    WHERE EXISTS (
        SELECT 1 FROM (VALUES (2, 'b'), (5, 'e')) AS batch(id, text)
        WHERE cte_tester.id = batch.id AND cte_tester.text = batch.text
    )
    RETURNING id
)
SELECT
    insert_ids.ids AS inserted,
    updates_ids.ids AS updated,
    deletes_ids.ids AS deleted
FROM (VALUES (1)) AS t(key)
INNER JOIN (
    SELECT 1 AS key, ARRAY_AGG(id) AS ids FROM inserts
) AS insert_ids ON t.key = insert_ids.key
INNER JOIN (
    SELECT 1 AS key, ARRAY_AGG(id) AS ids FROM updates
) AS updates_ids ON t.key = updates_ids.key
INNER JOIN (
    SELECT 1 AS key, ARRAY_AGG(id) AS ids FROM deletes
) AS deletes_ids ON t.key = deletes_ids.key;