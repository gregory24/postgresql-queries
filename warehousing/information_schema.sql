CREATE TABLE pk_tester(
    id SERIAL PRIMARY KEY,
    key VARCHAR
);

-- Columns and PK
SELECT
       c.column_name AS name,
       c.data_type AS type,
       (
        CASE c.is_nullable
        WHEN 'YES' THEN TRUE
        ELSE FALSE
        END
       ) AS nullable,
       (
        CASE tc.constraint_type
        WHEN 'PRIMARY KEY' THEN TRUE
        ELSE FALSE
        END
       ) AS pk
FROM information_schema.table_constraints AS tc
RIGHT JOIN information_schema.constraint_column_usage AS ccu
    USING (constraint_schema, constraint_name)
RIGHT JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
    AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
WHERE c.table_name = 'get_test_table';


-- Indexes
-- No way to determine ordering -> Specify in the name
--  -> Ordering may not exist for some indexes (Hash)
SELECT
    i.relname AS index_name,
    ix.indisunique AS is_unique,
    -- JSON is more compatible with Node.js (Automatically parsed)
    json_agg(a.attname) AS column_names
    -- Array is NOT parsed automatically (Not good)
    -- array_agg(a.attname) AS column_names
FROM
    pg_class t,
    pg_class i,
    pg_index ix,
    pg_attribute a,
    pg_namespace n
WHERE
    t.oid = ix.indrelid
    AND i.oid = ix.indexrelid
    AND a.attrelid = t.oid
    AND t.relnamespace = n.oid
    AND a.attnum = ANY(ix.indkey)
    AND t.relkind = 'r'
    AND t.relname = 'pk_tester'
    AND n.nspname = 'public'
    AND ix.indisprimary IS FALSE
GROUP BY i.relname, ix.indisunique
ORDER BY i.relname, ix.indisunique;

SELECT * FROM pg_indexes WHERE tablename = 'test' AND schemaname = 'public';