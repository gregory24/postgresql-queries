DROP TABLE IF EXISTS pagination_tester;

CREATE TABLE pagination_tester(
    id INTEGER NOT NULL,
    key1 INTEGER NOT NULL,
    key2 INTEGER NOT NULL,
    info TEXT NOT NULL
);

DROP INDEX IF EXISTS pagination_tester_idx;

TRUNCATE pagination_tester;

INSERT INTO pagination_tester(id, key1, key2, info)
SELECT i, i, i * 10, i::TEXT FROM generate_series(1, 1000 * 1000) AS series(i);

CREATE INDEX pagination_tester_idx ON pagination_tester USING BTREE (key1, key2);
