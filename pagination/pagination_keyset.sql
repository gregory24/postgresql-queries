-- Weight: 5.85 (x3500 improvement in terms of Disk-IO)

EXPLAIN
SELECT * FROM pagination_tester
WHERE (key1, key2) > (500000, 5000000)
ORDER BY key1, key2
LIMIT 100;
